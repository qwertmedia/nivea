<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Photo;

class PhotoController extends Controller
{
    public function uploadPhoto(Request $request, $type, $target)
    {
        if($request->ajax() && $request->hasFile('image')) {
			$photo = Photo::uploadPhoto($request->file('image'), $type);
            $target->photo()->delete();
            $target->photo()->save($photo);
            return response()->json($photo->getData());
		}
        return response()->json([]);
    }
    public function deletePhoto(Request $request, $photo)
    {
        if($request->ajax()) {
			$photo->delete();
		}
		return response()->json([]);
    }
}
