<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\User;
use App\Http\Requests\AdminTeamRequest;

class TeamController extends Controller
{
    public function getTeams()
    {
        return view('admin.teams', [
            'teams' => Team::all(),
            'statuses'=>array(1=>'on',0=>'off')
        ]);
    }
    public function getCreateTeam()
    {
        $statuses=array('register_status='=>0,'status'=>0);
        // var_dump($statuses->register_status);die;
        return view('admin.team', [
            'team' => new Team,
            'users' => User::all(),
            'statuses'=>array('register_status'=>0,'status'=>0),
        ]);
    }
    public function postCreateTeam(AdminTeamRequest $request)
    {
        $team = Team::createNew($request->except(['image', '_token']));
        foreach($team->players as $player) $player->gradePlayer();
        $team->backTeamMail();
        return redirect()->route('team', [$team->id]);
    }
    public function getTeam(Request $request, $team)
    {   
        return view('admin.team', [
            'team' => $team,
            'users' => User::all(),
            'statuses'=>Team::select('register_status','status')->where('id',$team->id)->first(),
        ]);
    }
    public function changeStatus(Request $request,$team,$status)
    {
        $team->updateStatus($status);
        return redirect()->route('teams');
    }
    public function changeRegisterStatus(Request $request,$team,$status)
    {
        $team->updateRegisterStatus($status);
        return redirect()->route('teams');
    }
    public function postTeam(AdminTeamRequest $request, $team)
    {
        foreach($team->players as $player) $player->degradePlayer();
        $team->updateTeam($request->input(['players'], []), $request->input('captain_id'),$request->input('status'),$request->input('register_status'));
        $team->update($request->except(['image', '_token']));
        foreach(Team::find($team->id)->players as $player) $player->gradePlayer();
        return redirect()->route('team', [$team->id]);
    }
    public function deleteTeam($team)
    {
        $team->captain->degradeCaptain();
        $team->delete();
        return redirect()->route('teams');
    }
}
