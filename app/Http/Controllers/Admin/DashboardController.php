<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Team;
use App\Models\Schelude;

class DashboardController extends Controller
{
    public function getDashboard()
    {
        return view('admin.dashboard',[
            'users' => User::all(),
            'teams' => Team::all(),
            'scheludes'=> Schelude::all(),
        ]);
    }
}
