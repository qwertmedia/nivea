<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use Auth;
use Hash;

class AuthController extends Controller
{
    public function getAuth()
    {
		return view('admin.auth');
	}
    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
    public function postAuth(UserLoginRequest $request)
    {
		if(Auth::attempt($request->only('email', 'password'), true)) {
            return redirect()->route('dashboard');
		} else {
			return redirect()->back()->withErrors(['message' => 'Invalid e-mail and/or password']);
		}
	}
}
