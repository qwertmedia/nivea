<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\Schelude;
use App\Http\Requests\AdminScheludeRequest;
use Google;
use Sheets;
class ScheludeController extends Controller
{
    public function index(){
    	return view('admin.scheludes', [
            'scheludes' => Schelude::all()
        ]);
    }
    public function getCreate()
    {
        return view('admin.schelude', [
            'schelude' => new Schelude
        ]);
    }
    public function postCreate(AdminScheludeRequest $request)
    {	
        $model=Schelude::createNew($request->input('team1_id'),$request->input('team2_id'),$request->input('score'),$request->input('date'),$request->input('group_stage'),$request->input('comment'));

        return redirect()->route('schelude', [$model->id]);
    }

    public function postSchelude(AdminScheludeRequest $request,$id)
    {
        $team->updateTeam($team1_id, $team2_id,$score,$date,$group_stage);
        return redirect()->route('schelude', [$id]);
    }
    public function getSchelude($id)
    {
        $schelude = Schelude::find($id);
        return view('admin.schelude', [
            'schelude' => $schelude
        ]);
    }
    public function deleteSchelude($id)
    {
        Schelude::find($id)->delete();
        return redirect()->route('scheludes');
    }
    public function importSchelude(){
        Sheets::setService(Google::make('sheets'));
        Sheets::spreadsheet('1sFpE-F9dDE7WIIiNXl-svlM3vSWnqPlgdgvsmmJ6kXc');
        $values = Sheets::sheet('players')->all();
        foreach (array_slice($values, 1) as $key => $value) {
            var_dump($value);
            if(!empty($value)){
                $id1=explode('tm_', $value[2]);
                $id2=explode('tm_', $value[6]);    
                // var_dump($id2);die;        
                Schelude::createNew($id1[1],$id2[1],$value[4].":".$value[5],$value[1],$value[8],isset($value[9])&&!empty($value[9])?$value[9]:"");
            }
        }
        return redirect()->route('scheludes');
    }
}
