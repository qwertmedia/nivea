<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Bican\Roles\Models\Role;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Sheets;
use App\Models\Team;
use Mail;
//use Google;
use Hashids;
use DB;
use Excel;
use App\Http\Requests\AdminUserRequest;

class UserController extends Controller
{
    public function getUsers()
    {
        return view('admin.users', [
            'users' => User::all()
        ]);
    }
    public function getCreateUser()
    {
        return view('admin.user', [
            'user' => new User,
            'roles' => Role::where('slug', '!=', 'superadmin')->get()
        ]);
    }
    public function postCreateUser(AdminUserRequest $request)
    {
        $user = User::createNew($request->except(['image', '_token']));
        return redirect()->route('user', [$user->id]);
    }
    public function getUser(Request $request, $user)
    {
        return view('admin.user', [
            'user' => $user,
            'roles' => Role::where('slug', '!=', 'superadmin')->get()
        ]);
    }
    public function postUser(AdminUserRequest $request, $user)
    {
        $birthday = $request->input('year') . '-' . $request->input('month') . '-' . $request->input('day');
        $user->update(array_merge($request->except(['image', '_token']), ['birthday' => $birthday]));
        if(!$user->is('superadmin'))
            $user->syncRoles($request->input('roles', []));
        return redirect()->route('user', [$user->id]);
    }


    public function deleteUser($user)
    {
        $user->delete();
        return redirect()->route('users');
    }
    public function deleteDuplicate(){
        $users=DB::select("
    SELECT DISTINCT user_id FROM play_role_user
    WHERE user_id
    IN (
    SELECT user_id
    FROM play_role_user
    GROUP BY user_id
    HAVING COUNT( user_id ) > 1
    )
    ");
        // var_dump($users);
        $ids=array();
        foreach ($users as $key => $value) {
           $ids[]=$value->user_id;
        }
        $users=DB::table('role_user')->where('role_id', '=', 5)
                    ->whereIn('role_user.user_id',$ids)
                    // ->where('users.birthday','<','CURDATE() ')
                    ->delete();
    }
    public function importTeamsDocs() {

       Excel::create('Teams', function($excel) {

            // Our first sheet
            $excel->sheet('Teams', function($sheet) {
                 
                $exp_users=array();
                $ids=array();
                $tmp_arr=array();
                // $team_ids=DB::select("SELECT *, COUNT('tp.user_id') players FROM `play_users` pu LEFT JOIN `play_team_players` AS tp ON `pu`.`id` = `tp`.`user_id` LEFT JOIN `play_teams` AS pt ON `pt`.`id` = `tp`.`team_id` LEFT JOIN `play_role_user` AS ru ON ru.user_id = pu.id WHERE ru.role_id = 3 GROUP BY pu.email HAVING COUNT(tp.user_id >= 10) ORDER BY `pu`.`id` ASC  ");
                $team_ids=DB::table('users')
                    ->leftJoin('team_players', 'users.id', '=', 'team_players.user_id')
                    ->leftJoin('teams', 'teams.id', '=', 'team_players.team_id')
                    ->leftJoin('role_user', 'role_user.user_id','=','users.id')
                    ->select('*')
                    ->selectRaw('count("ptp.players")')
                    // ->where('uzsers.birthday','>','1982')
                    ->groupBy('team_players.team_id')
                    // ->havingRaw('COUNT(play_team_players.user_id) >= 10')
                    ->get();
                foreach ($team_ids as $key => $value) {
                        $pl=DB::select("SELECT count(pu.id) as not_signed from play_role_user pr LEFT JOIN play_users as pu on pu.id=pr.user_id LEFT JOIN play_team_players as tp on tp.user_id=pu.id LEFT JOIN play_teams as pt on tp.team_id=pt.id WHERE role_id=4 and pt.id=".(!empty($value->team_id)?$value->team_id:0));
                        $tmp_arr[]=array(
                            'team_id'=>'tm_'.$value->team_id,
                            'team_name'=>$value->name,
                            'players'=>$value->players,
                            'not signed'=>isset($pl[0]->not_signed)&&!empty($pl[0]->not_signed)?$pl[0]->not_signed:"",
                            'first_name'=>$value->first_name,
                            'surname'=>$value->surname,
                            'last_name'=>$value->last_name,
                            'email'=>$value->email,
                            'phone'=>$value->phone,
                            'birthday'=>$value->birthday,
                            'link' =>'http://ext14.niveamen.ua/23/play_root/public/#/team/'.Hashids::encode($value->team_id)

                        );
                }

                $sheet->fromArray($tmp_arr,null,'A1',false,true);
            });
            })->export('xls');
    }
    public function importTeamUsers(){
        
        
        // var_dump($ids);
        Excel::create('TeamPlayers', function($excel) {

            // Our first sheet
            $excel->sheet('Players', function($sheet) {   
                $exp_users=array();
                $team_ids=DB::select("SELECT pt.id FROM `play_role_user` ru LEFT JOIN play_users as pu on pu.id=ru.user_id LEFT JOIN play_team_players as tp on tp.user_id=pu.id LEFT JOIN play_teams as pt on pt.id=tp.team_id group by pt.id having count(tp.user_id) >=10");
                foreach ($team_ids as $id){
                    $ids[]=$id->id;
                }
                
                $users=DB::table('users')
                    ->leftJoin('team_players', 'users.id', '=', 'team_players.user_id')
                    ->leftJoin('teams', 'teams.id', '=', 'team_players.team_id')
                    ->leftJoin('role_user','role_user.user_id','=','users.id')
                    ->select('users.email','users.phone','users.birthday','users.first_name','users.last_name','users.surname','role_user.role_id' ,'teams.captain_id','team_players.user_id','team_players.team_id','teams.name')
                    ->selectRaw('COUNT(play_team_players.user_id) players')
                    // ->selectRaw('DISTINCT team_players.user_id as user_id')
                    ->whereIn('team_players.team_id',$ids)
                    ->where('users.birthday','>=','1982-01-01' )
                     ->groupBy('team_players.team_id')
                    ->get();
                foreach ($users as $value){
                   $pl=DB::select("SELECT count(pu.id) as not_signed from play_role_user pr LEFT JOIN play_users as pu on pu.id=pr.user_id LEFT JOIN play_team_players as tp on tp.user_id=pu.id LEFT JOIN play_teams as pt on tp.team_id=pt.id WHERE role_id=4 and pt.id=".(!empty($value->team_id)?$value->team_id:0));
                    $exp_users[]=array(
                            'team_id'=>'tm_'.$value->team_id,
                            'player_id'=>'tm_'.$value->team_id.'_pl_'.$value->user_id,
                            'players'=>$value->players,
                            'not signed'=>isset($pl[0]->not_signed)&&!empty($pl[0]->not_signed)?$pl[0]->not_signed:"",
                            'team_name'=>$value->name,
                            'first_name'=>$value->first_name,
                            'surname'=>$value->surname,
                            'last_name'=>$value->last_name,
                            'email'=>$value->email,
                            'phone'=>$value->phone,
                            'birthday'=>$value->birthday,
                            'role'=>$value->user_id==$value->captain_id?"cap":"pla"

                        );   
                }
                $sheet->fromArray(($exp_users),null,'A1',false,true);
            });
        })->export('xls');
    }
    public function importUsersDocs(){

        

        Excel::create('Players', function($excel) {

            // Our first sheet
            $excel->sheet('Players', function($sheet) {   
            $exp_users=array();
            $ids=array();
            // $team_ids=DB::table('users')
            //     ->leftJoin('team_players', 'users.id', '=', 'team_players.user_id')
            //     ->leftJoin('teams', 'teams.id', '=', 'team_players.team_id')
            //     ->select('team_players.team_id')
            //     ->selectRaw("COUNT('team_players.user_id') as count")
            //     // ->where('users.birthday','>','1982')
            //     // ->orWhere('users.email','LIKE','%n%')
            //     // ->orWhere('users.phone','LIKE','%39%')
            //     // ->orWhere('users.phone','LIKE','%67%')
            //     // ->orWhere('users.phone','LIKE','%68%')
            //     // ->orWhere('users.phone','LIKE','%96%')
            //     // ->orWhere('users.phone','LIKE','%97%')
            //     // ->orWhere('users.phone','LIKE','%98%')
            //     ->groupBy('team_players.team_id')
            //    ->havingRaw('COUNT(play_team_players.user_id) >= 10')
            //     ->get();
//            die;
            // $users=DB::select("SELECT * FROM `play_role_user` ru LEFT JOIN play_users as pu on pu.id=ru.user_id LEFT JOIN play_team_players as tp on tp.user_id=pu.id LEFT JOIN play_teams as pt on pt.id=tp.team_id WHERE ru.role_id=5 OR ru.role_id=3 group by ru.team_id having count(ru.role_id)>");

            $team_ids= DB::select("SELECT *
                FROM play_users pu 
                LEFT JOIN play_team_players AS tp ON tp.user_id = pu.id 
                LEFT JOIN play_teams AS pt ON pt.id = tp.team_id 
                LEFT JOIN play_role_user AS ru ON ru.user_id=pu.id
                -- GROUP BY tp.team_id 
                -- HAVING COUNT(tp.user_id)>=10 
                -- and AVG( (YEAR(NOW())-Year(pu.birthday))- (date_format(NOW(), '%m%d') < date_format(pu.birthday, '%m%d')) )<= 34
                ");
            // var_dump($team_ids);die;
            // // var_dump(count($team_ids));die;
            // foreach ($team_ids as $id){
            //     $ids[]=$id->team_id;
            // }

                // $users=DB::table('users')
                //     ->leftJoin('team_players', 'users.id', '=', 'team_players.user_id')
                //     ->leftJoin('teams', 'teams.id', '=', 'team_players.team_id')
                //     ->leftJoin('role_user','role_user.user_id','=','users.id')
                //     ->selectRaw('COUNT(play_team_players.user_id)')
                //     ->select('role_user.role_id','users.*', 'teams.captain_id','team_players.user_id','team_players.team_id','teams.name')
                //     ->whereIn('team_players.team_id',$ids)
                //     ->where('users.birthday','>','1982-01-01' )
                //     // ->where('users.birthday','<','CURDATE() ')
                //     ->get();
                
                foreach ($team_ids as $value){
                
                    $exp_users[]=array(
                            'team_id'=>'tm_'.str_pad($value->team_id, 4, '0', STR_PAD_LEFT),
                            'player_id'=>'tm_'.str_pad($value->team_id, 4, '0', STR_PAD_LEFT).'_pl_'.str_pad($value->id, 4, '0', STR_PAD_LEFT),
                            'team_name'=>$value->name,
                            'first_name'=>$value->first_name,
                            'surname'=>$value->surname,
                            'last_name'=>$value->last_name,
                            'email'=>$value->email,
                            'phone'=>$value->phone,
                            'birthday'=>$value->birthday,
                            'role'=>$value->id==$value->captain_id?"cap":"pla",
                            'status'=>$value->role_id==3||$value->role_id==5?"in_team":"pending"

                        );   
                }
                // var_dump($exp_users);die;
                $sheet->fromArray(($exp_users),null,'A1',false,true);
            });
            })->export('xls');
    }
    public function sendEmailTeams(){
        /**/
    $teams=DB::select('SELECT count(ptp.user_id) players,pt.captain_id FROM play_team_players ptp LEFT JOIN play_teams as pt on pt.id=ptp.team_id LEFT JOIN play_users as pu on pu.id=ptp.user_id GROUP BY ptp.team_id HAVING COUNT(ptp.user_id) <10 ');
    $captains=array();
    foreach ($teams as $key => $value) {
        array_push($captains,$value->captain_id);
    }
    $ids = join(",",$captains); 

    $get_emails=DB::select('SELECT pu.email, pu.link FROM play_users pu LEFT JOIN play_team_players as ptp on ptp.user_id=pu.id WHERE pu.id IN (  '.$ids.')');

    foreach ($get_emails as $value) {
            Mail::queue('emails.captain.collect_players', array('link'=>$value->link), function($message) use ($value) {
                $message->from('play-noreply@nivea.kiev.ua', 'NIVEA MEN');
                $message->to($value->email);
                $message->subject('Недостатньо гравців. Будь ласка, укомплектуйте команду. Футбольний турнір NIVEA MEN 2016.');
            });
    }
    }
    public function importUsersCsv(){
 


        // output headers so that the file is downloaded rather than displayed
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=data.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        $players=array();
        fputcsv($output, array('id','firstname','lastname','surname','team','birthday','phone','email'),';');
        foreach (User::all() as $one){
            $team="";
            
            if(isset($one->team->name) && !empty($one->team->name))
                $team=$one->team->name;
            $tmp=array(
                'id'=>iconv("utf-8", "windows-1251", $one->id),
                'team'=>iconv("utf-8", "windows-1251//TRANSLIT", $team),
                'firstname'=>iconv("utf-8", "windows-1251", $one->first_name),
                'lastname'=>iconv("utf-8", "windows-1251", $one->last_name),
                'surname'=>iconv("utf-8", "windows-1251", $one->surname),
                'team'=>iconv("utf-8", "windows-1251//TRANSLIT", $team),
                'birthday'=>date('d-m-Y', strtotime($one->birthday) ),
                'phone'=>iconv("utf-8", "windows-1251", $one->phone),
                'email'=>iconv("utf-8", "windows-1251", $one->email)
            );
            fputcsv($output, $tmp,';');
            }
        fclose($output);
        ob_get_clean();
        die;
       }
        
}
