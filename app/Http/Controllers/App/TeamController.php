<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use Bican\Roles\Models\Role;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TeamRequest;
use App\Http\Requests\TeamPlayerRequest;
use App\Http\Requests\TeamPlayerUpdateRequest;
use App\Models\User;
use App\Models\Team;
use Auth;

class TeamController extends Controller
{
    public function postTeam(TeamRequest $request)
    {
        if($request->ajax()) {
            $roles = Role::whereIn('slug', ['user', 'captain'])->select(['id'])->get();
            $password = $request->password;
            $user = User::createNew(array_merge($request->all(), [
                'roles' => $roles
            ]));
            Auth::login($user);
            $team = Team::createNew(array_merge($request->only('name'), [
                'players' => [],
                'captain_id' => $user->id
            ]));
            $team->frontTeamMail($password);
            return response()->json(User::with('teamAsCaptain')->find($user->id));
        }
        return response()->json([]);
    }
    public function getTeams()
    {
        return Team::with('photo', 'players', 'captain')->paginate();
    }
    public function getTeam(Request $request, $id)
    {
        return response()->json(Team::with('players', 'captain')->with(['players.roles' => function($q) {
            return $q->select('slug');
        }])->find($id));
    }
    public function getRemove(Request $request)
    {
        $link = $request->input('link');
        $team = Team::where('link', $link)->first();
        $status = '';
        if($request->input('approved', false)) {
            if($team->players()->count()) return back();
            $team->delete();
            $team->captain()->delete();
            $status = 'success';
        }
        return view('team.remove', [
            'team' => $team,
            'status' => $status
        ]);
    }
    public function getSearch($name)
    {
        if(isset($name) && !empty($name))
            return Team::with('photo', 'players', 'captain')->where('name','LIKE','%'.$name.'%')->get();
    }

}
