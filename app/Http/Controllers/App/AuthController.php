<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\UserLoginRequest;
use Auth;
use Hash;
use App\Models\User;

class AuthController extends Controller
{
    public function getUser()
    {
        if(Auth::check()) {
            //dd(Auth::user()->with('teamAsCaptain'));
            return response()->json(User::with('teamAsCaptain')->find(Auth::user()->id));
        } else {
            return response()->json([], 401);
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return response()->json();
    }

    public function postLogin(UserLoginRequest $request)
    {
        if(Auth::attempt($request->only('email', 'password'), true)) {
            $user = Auth::user();
            if(object_get($user->teamAsCaptain, 'id', false)) {
                return $this->getUser();
            } else {
                return $this->getLogout();
            }
		}

        $user = User::where('email', $request->input('email'))->first();
        if($user) {
		  return response()->json(['password' => 'wrong password'], 401);
        }
        return response()->json(['email' => 'email not found'], 401);
    }

    public function postReset(Request $request)
    {
        $user = User::where('email', $request->input('email'))->first();
        if($user) {
            $user->resetPassword();
            return response()->json();
        }
        return response()->json(['email' => 'email not found'], 422);
    }
}
