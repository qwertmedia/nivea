<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use Bican\Roles\Models\Role;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TeamPlayerRequest;
use App\Http\Requests\TeamPlayerUpdateRequest;
use App\Models\User;
use Redirect;

class UserController extends Controller
{
    public function getRemove(Request $request)
    {
        $link = $request->input('link');
        $user = User::where('link', $link)->first();
        $status = '';
        if($request->input('approved')) {
            $user->delete();
            $status = 'success';
        }
        return view('user.remove', [
            'user' => $user,
            'status' => $status
        ]);

    }

    public function postTeamPlayer(TeamPlayerRequest $request, $team)
    {
        $role = Role::where('slug', 'user')->first();
        $user = User::createNew(array_merge($request->all(), [
            'roles' => [$role->id]
        ]));
        $team->addToTeam($user);
        return response()->json($user);
    }

    public function putTeamPlayer(TeamPlayerUpdateRequest $request, $team, $player)
    {
        $player->update($request->all());
        return response()->json(User::with(['roles' => function($q) {
            return $q->select('slug');
        }])->find($player->id));
    }

    public function deleteTeamPlayer(Request $request, $team, $player)
    {
        $player->delete();
        return response()->json([]);
    }

    public function postAcceptTeamPlayer(Request $request, $team, $player)
    {
        $player->gradePlayer();
        $player->acceptPlayerMail($team);
        return response()->json(User::with(['roles' => function($q) {
            return $q->select('slug');
        }])->find($player->id));
    }

    public function postDeclineTeamPlayer(Request $request, $team, $player)
    {
        $player->declinePlayerMail($team);
        $player->delete();
        return response()->json(User::with(['roles' => function($q) {
            return $q->select('slug');
        }])->find($player->id));
    }
}
