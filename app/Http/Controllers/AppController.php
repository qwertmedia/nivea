<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


class AppController extends Controller
{
    public function getApp()
    {
        return view('app');
    }
    public function getIframe()
    {
        return view('iframe');
    }


}
