<?php

Route::get('auth', ['as' => 'login', 'uses' => 'Admin\AuthController@getAuth']);
Route::post('auth', ['as' => 'login', 'uses' => 'Admin\AuthController@postAuth']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Admin\AuthController@getLogout']);

Route::get('/', ['as' => 'app', 'uses' => 'AppController@getApp']);
Route::get('/iframe', [ 'uses' => 'AppController@getIframe']);

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:superadmin|admin']], function () {
    Route::get('/', ['as' => 'dashboard', 'uses' => 'Admin\DashboardController@getDashboard']);
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', ['as' => 'users', 'uses' => 'Admin\UserController@getUsers']);
        Route::get('/create', ['as' => 'create-user', 'uses' => 'Admin\UserController@getCreateUser']);
        Route::post('/create', ['as' => 'create-user', 'uses' => 'Admin\UserController@postCreateUser']);
        Route::get('/view/{user}', ['as' => 'user', 'uses' => 'Admin\UserController@getUser']);
        Route::post('/view/{user}', ['as' => 'update-user', 'uses' => 'Admin\UserController@postUser']);
        Route::get('/delete/{user}', ['as' => 'delete-user', 'uses' => 'Admin\UserController@deleteUser']);
        Route::get('/importcsv/', ['as' => 'import-users-csv', 'uses' => 'Admin\UserController@importUsersCsv']);
        Route::get('/importusersdocs/', ['as' => 'import-users-docs', 'uses' => 'Admin\UserController@importUsersDocs']);
        Route::get('/importteamsdocs/', ['as' => 'import-users-docs', 'uses' => 'Admin\UserController@importTeamsDocs']);
        Route::get('/importteamplayers/', ['as' => 'import-team-users', 'uses' => 'Admin\UserController@importTeamUsers']);


    });

    Route::group(['prefix' => 'teams'], function () {
        Route::get('/', ['as' => 'teams', 'uses' => 'Admin\TeamController@getTeams']);
        Route::get('/create', ['as' => 'create-team', 'uses' => 'Admin\TeamController@getCreateTeam']);
        Route::post('/create', ['as' => 'create-team', 'uses' => 'Admin\TeamController@postCreateTeam']);
        Route::get('/view/{team}', ['as' => 'team', 'uses' => 'Admin\TeamController@getTeam']);
        Route::post('/view/{team}', ['as' => 'update-team', 'uses' => 'Admin\TeamController@postTeam']);
        Route::get('/delete/{team}', ['as' => 'delete-team', 'uses' => 'Admin\TeamController@deleteTeam']);
        Route::get('/changestatus/{team}/{status}', ['as' => 'change-status-team', 'uses' => 'Admin\TeamController@changeStatus']);
        Route::get('/changeregisterstatus/{team}/{status}', ['as' => 'change-status-team', 'uses' => 'Admin\TeamController@changeRegisterStatus']);

    });
    Route::group(['prefix' => 'scheludes'], function () {
        Route::get('/', ['as' => 'scheludes', 'uses' => 'Admin\ScheludeController@index']);
        Route::get('/create', ['as' => 'create-schelude', 'uses' => 'Admin\ScheludeController@getCreate']);
        Route::post('/create', ['as' => 'create-schelude', 'uses' => 'Admin\ScheludeController@postCreate']);
        Route::get('/view/{id}', ['as' => 'schelude', 'uses' => 'Admin\ScheludeController@getSchelude']);
        Route::post('/view/{id}', ['as' => 'update-schelude', 'uses' => 'Admin\ScheludeController@getSchelude']);
        Route::get('/delete/{id}', ['as' => 'delete-schelude', 'uses' => 'Admin\ScheludeController@deleteSchelude']);
        Route::get('/import', ['as' => 'import-schelude', 'uses' => 'Admin\ScheludeController@importSchelude']);

    });
    Route::group(['prefix' => 'photo'], function () {
        Route::post('/upload/{type}/{team}', ['as' => 'photo-team-upload', 'uses' => 'Admin\PhotoController@uploadPhoto'])
            ->where('type', 'team');
        Route::post('/upload/{type}/{user}', ['as' => 'photo-user-upload', 'uses' => 'Admin\PhotoController@uploadPhoto'])
            ->where('type', '(team|user)');
        Route::post('/delete/{photo}', ['as' => 'admin-photo-delete', 'uses' => 'Admin\PhotoController@deletePhoto']);

    });
});

Route::group(['prefix' => 'api/v1', 'middleware' => ['api']], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::get('/', 'App\AuthController@getUser');
        Route::get('/logout', 'App\AuthController@getLogout');
        Route::post('/login', 'App\AuthController@postLogin');
        Route::post('/reset', 'App\AuthController@postReset');
    });
    Route::get('/teams', 'App\TeamController@getTeams');
    Route::get('/teams/{id}', 'App\TeamController@getTeam');
    Route::get('/teams/search/{name}', ['as' => 'search-teams', 'uses' => 'App\TeamController@getSearch']);


    Route::post('/teams', 'App\TeamController@postTeam');
    Route::post('/teams/{team}/players', 'App\UserController@postTeamPlayer');
    Route::group(['middleware' => ['auth']], function () {
        Route::post('/teams/{team}/players/{user}', 'App\UserController@putTeamPlayer');
        Route::group(['middleware' => ['role:superadmin|admin|captain']], function () {
            Route::delete('/teams/{team}/players/{user}', 'App\UserController@deleteTeamPlayer');
            Route::post('/teams/{team}/players/{user}/accept', 'App\UserController@postAcceptTeamPlayer');
            Route::post('/teams/{team}/players/{user}/decline', 'App\UserController@postDeclineTeamPlayer');
        });
    });
});
Route::group(['prefix' => 'remove'], function () {
    Route::get('user', ['as' => 'remove-user', 'uses' => 'App\UserController@getRemove']);
    Route::get('team', ['as' => 'remove-team', 'uses' => 'App\TeamController@getRemove']);
});
