<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TeamPlayerUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         $user = $this->route('user');
         return [
             'phone' => 'required',
             'email' => 'required|unique:users,email,' . object_get($user, 'id', 0),
         ];
     }
}
