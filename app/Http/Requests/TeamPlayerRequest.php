<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TeamPlayerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
     {
         return true;
     }

     /**
      * Get the validation rules that apply to the request.
      *
      * @return array
      */
     public function rules()
     {
         $user = $this->route('user');
         return [
             'first_name' => 'required',
             'last_name' => 'required',
             'surname' => 'required',
             'phone' => 'required',
             'day' => 'required',
             'month' => 'required',
             'year' => 'required',
             'email' => 'required|unique:users,email,' . object_get($user, 'id', 0),
             'rules' => 'required',
         ];
     }
}
