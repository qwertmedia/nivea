<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user');
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'surname' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:users,email,' . object_get($user, 'id', 0),
            'password' => 'confirmed' . (object_get($user, 'id', false) ?: '|required'),
            'password_confirmation' => 'required_with:password'
        ];
    }
}
