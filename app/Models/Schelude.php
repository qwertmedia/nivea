<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Team;
class Schelude extends Model
{
    public $timestamps = false;
    public  static function createNew($team1_id,$team2_id,$score,$date,$group_stage,$comment){
        $model= new Schelude();
        $model->team1_id=$team1_id;
        $model->team2_id=$team2_id;
        $model->team1_name=Team::find($team1_id)->name?Team::find($team1_id)->name:'wrong id';
        $model->team2_name=Team::find($team2_id)->name?Team::find($team2_id)->name:'wrong id';
        $model->score=$score;
        $model->date=$date;
        $model->comment=$comment;
        $model->group_stage=$group_stage;
        $model->save();
        return $model;
    }
}
