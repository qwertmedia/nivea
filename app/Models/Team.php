<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Mail;
use Hashids;

class Team extends Model
{
    protected $fillable = ['name', 'captain_id', 'link'];
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function players()
    {
        return $this->belongsToMany('App\Models\User', 'team_players');
    }

    public function photo() {
		return $this->morphOne('App\Models\Photo', 'owner')->where('type', '=', 'photo');
	}
    public function captain()
    {
        return $this->hasOne('App\Models\User', 'id', 'captain_id');
    }

    public static function createNew($data)
    {
        $team = Team::create($data);
        $team->updateTeam(array_get($data, 'players', []), array_get($data, 'captain_id'),1,1);
        return $team;
    }
    public function updateStatus($status)
    {
        $this->status=$status;
        $this->save();   
    }
    public function updateRegisterStatus($status)
    {
        $this->register_status=$status;
        $this->save();   
    }
    public function updateTeam($players, $captain,$status,$register_status)
    {
        $players = array_diff($players, [$captain]);
        $newCaptain = User::find($captain);
        if(object_get($this->captain, 'id', -1) != $captain) {
            if($this->captain) {
                $this->captain->degradeCaptain();
            }
            $newCaptain->gradeCaptain();
        }
        if(!$this->captain->is('captain')){
            $newCaptain->gradeCaptain();
        }
        // var_dump($status);die;
        $this->status=$status;
        $this->register_status=$register_status;
        $this->players()->sync($players);
    }
    public function addToTeam($player)
    {
        if($this->players()->count() != env('MAX_TEAM_PLAYERS', 14)) {
            $this->players()->attach($player->id);
            //$player->gradePlayer();
        }

        $this->captain->captainNewPlayerMail($this, $player);
        $player->welcomeUserMail($this);
    }
    public function getTeamLink() {
        return env('BASE_URL', env('APP_URL')) . '/#/team/' .  Hashids::encode($this->id);
    }
    public function getAppTeamLink() {
        return env('BASE_URL', env('APP_URL')) . '/#/app/team/' .  Hashids::encode($this->id);
    }

    public function frontTeamMail($password = null)
    {
        $this->captain->welcomeCaptainFrontMail($this, $password);
    }
    public function backTeamMail()
    {
        $this->captain->welcomeCaptainBackMail($this);
    }

    public function getDeclineLink() {
        return env('REMOVE_INST_URL') . '/remove/team?link=' . $this->link;
    }
    public function onSaving()
    {
    }
    public function onCreating()
    {
        $this->link = str_random(32);
    }
    public function onCreated()
    {

    }
    public function onUpdating()
    {
    }
    public function onDeleting()
    {
        $this->captain->degradeCaptain();
        $this->photo()->delete();
    }

}
