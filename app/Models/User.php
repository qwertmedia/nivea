<?php

namespace App\Models;

use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Bican\Roles\Models\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use Mail;

class User extends Authenticatable implements HasRoleAndPermissionContract
{
    use HasRoleAndPermission;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'surname', 'phone',  'email', 'password', 'birthday', 'about'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at',
    ];

    protected $appends = ['in_team', 'team'];

    private function teams()
    {
        return $this->belongsToMany('App\Models\Team', 'team_players');
    }

    public function photo()
    {
        return $this->morphOne('App\Models\Photo', 'owner')->where('type', '=', 'photo');
    }
    public function getInTeamAttribute()
    {
        return $this->is('player') || $this->is('captain');
    }
    public function getTeamAttribute()
    {
        return $this->teams()->first();
    }
    public function teamAsCaptain()
    {
        return $this->belongsTo('App\Models\Team', 'id', 'captain_id');
    }

    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }
    public function getSplitBirthdayAttribute()
    {
        return explode('-', $this->birthday);
    }
    public static function createNew($data)
    {
        $date = array_get($data, 'year') . '-' . array_get($data, 'month') . '-' . array_get($data, 'day');
        $user = self::create(array_merge($data, ['birthday' => $date]));
        $user->syncRoles(array_get($data, 'roles', []));

        return $user;
    }

    public function syncRoles($roles)
    {
        $this->roles = null;

        return empty($roles) ? $this->detachAllRoles() : $this->roles()->sync($roles);
    }
    public function gradeCaptain()
    {
        $captainRole = Role::where('slug', 'captain')->first();
        $this->attachRole($captainRole);
    }
    public function degradeCaptain()
    {
        $captainRole = Role::where('slug', 'captain')->first();
        $this->detachRole($captainRole);
    }

    public function gradePlayer()
    {
        $playerRole = Role::where('slug', 'player')->first();
        $this->attachRole($playerRole);
    }
    public function degradePlayer()
    {
        $playerRole = Role::where('slug', 'player')->first();
        $this->detachRole($playerRole);
    }

    public function onSaving()
    {
        $this->phone = str_replace(' ', '', $this->phone);
    }
    public function onCreating()
    {
        $this->password = Hash::make($this->password);
        $this->link = str_random(32);
    }
    public function onUpdating()
    {
    }
    public function onDeleting()
    {
        $this->photo()->delete();
    }
    public function getDeclineLink() {
        return env('REMOVE_INST_URL') . '/remove/user?link=' . $this->link;
    }
    // (Максим) (Актуальные письма = п.5 Сделал)
    public function acceptPlayerMail($team)
    {
        $this->sendMail('emails.player.accept', [
            'link' => $team->getTeamLink(),
            'team' => $team,
            'subject' => 'Вітаємо, Ви в команді! Футбольний турнір NIVEA MEN 2016.',
        ]);
    }
    // (Максим) (Актуальные письма = п.6 Сделал)
    public function declinePlayerMail($team)
    {
        $this->sendMail('emails.player.decline', [
            'link' => $team->getTeamLink(),
            'subject' => 'Ваш запит на участь в команді відхилено. Футбольний турнір NIVEA MEN 2016.',
        ]);
    }
    // (Максим) (Актуальные письма = п.2 Сделал)
    public function welcomeCaptainFrontMail($team, $password = null)
    {
        $this->sendMail('emails.team.new', [
            'password'=>$password,
            'user' => $this,
            'link' => $team->getTeamLink(),
            'linkApp' => $team->getAppTeamLink(),
            'linkDecline' => $team->getDeclineLink(),
            'subject' => 'Заявочний лист Вашої команди зареєстровано. Футбольний турнір NIVEA MEN 2016.',
        ]);
    }
    // (Максим) (Актуальные письма = п.1 Сделал)
    public function welcomeCaptainBackMail($team)
    {
        $this->sendMail('emails.team.new_back', [
            'user' => $this,
            'link' => $team->getTeamLink(),
            'linkApp' => $team->getAppTeamLink(),
            'linkDecline' => $team->getDeclineLink(),
            'subject' => 'Ваша Заявка на участь у турнірі NIVEA MEN 2016',
        ]);
    }

    // (Максим) (Актуальные письма = п.4 Сделал)
    public function welcomeUserMail($team)
    {
        $this->sendMail('emails.player.welcome', [
            'link' => $team->getTeamLink(),
            'linkDecline' => $this->getDeclineLink(),
            'team' => $team,
            'subject' => 'Ваш запит на участь в команді відправлено. Футбольний турнір NIVEA MEN 2016.',
        ]);
    }
    // (Максим) (Актуальные письма = п.3 Сделал)
    public function captainNewPlayerMail($team, $user)
    {
        $this->sendMail('emails.captain.new_player', [
            'user' => $user,
            'link' => $team->getTeamLink(),
            'subject' => $user->first_name.' '. $user->last_name. ' хоче грати за Вашу команду в турнірі NIVEA MEN 2016',
        ]);
    }
    public function resetPassword()
    {
        $newPassword = str_random(8);
        $this->update(['password' => Hash::make($newPassword)]);
        $this->sendMail('emails.captain.reset', [
            'user' => $this,
            'password' => $newPassword,
            'subject' => 'Восстановление пароля Футбольный турнир NIVEA MEN',

        ]);
    }


    private function sendMail($teamplate, $data)
    {
        Mail::queue($teamplate, $data, function ($message) use($data) {
            $message->from('play-noreply@NIVEA.kiev.ua', 'Nivea men');
            $message->to($this->email, $this->first_name . ' ' . $this->last_name)
                ->subject($data['subject']);
        });
    }
}
