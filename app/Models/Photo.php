<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Image;
use Storage;

class Photo extends Model
{
    protected $fillable = ['name', 'original_name', 'path', 'type', 'meta_title', 'meta_alt'];
    protected $hidden = ['owner_id', 'owner_type', 'created_at', 'updated_at'];

    public function owner()
    {
        return $this->morphTo();
    }
    public function delete()
    {
        if(Storage::exists($this->path . $this->name)) {
            Storage::delete($this->path . $this->name);
        }
        return parent::delete();
    }

    public function getData()
    {
        return [
            'initialPreview' => [
                view('admin.photo_preview', ['photo' => $this])->render()
            ],
            'initialPreviewConfig' => [
                [
                    'caption' => $this->original_name,
                    'url' => route('admin-photo-delete', $this->id),
                    'key' => $this->id
                ]
            ]
        ];
    }

    public static function uploadPhoto($image, $type)
    {
        $img = Image::make($image);
        $path = "public/{$type}/";
        if(!Storage::exists($path)) {
            Storage::makeDirectory($path);
        }
        $photoName = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();

        $img->save(storage_path('app') . "/" . $path . $photoName, 100);

        $photo = Photo::create([
            'name' => $photoName,
            'original_name' => $image->getClientOriginalName(),
            'path' => $path,
            'meta_alt' => $image->getClientOriginalName(),
            'meta_title' => $image->getClientOriginalName(),
            'type' => 'photo'
        ]);

        return $photo;
    }
}
