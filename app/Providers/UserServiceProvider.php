<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\User;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::creating(function($user) {
            $user->onCreating();
        });
        User::updating(function($user) {
            $user->onUpdating();
        });
        User::saving(function($user) {
            $user->onSaving();
        });
        User::deleting(function($user) {
            $user->onDeleting();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
