<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\Team;

class TeamServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Team::creating(function($team) {
            $team->onCreating();
        });
        Team::created(function($team) {
            $team->onCreated();
        });
        Team::updating(function($team) {
            $team->onUpdating();
        });
        Team::saving(function($team) {
            $team->onSaving();
        });
        Team::deleting(function($team) {
            $team->onDeleting();
        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
