System.import('noty').then(() => {

    $.extend($.noty.defaults, {
        layout: 'topRight',
        theme: 'bootstrapTheme',
        timeout: 5000
    });

    if(typeof notificationErrors !== 'undefined') {
        for(let error of notificationErrors) {
            noty({
                text: error,
                type: 'error'
            });
        }
    }
    if(typeof notificationWarnings !== 'undefined') {
        for(let warning of notificationWarnings) {
            noty({
                text: warning,
                type: 'warning'
            });
        }
    }
    if(typeof notificationInformations !== 'undefined') {
        for(let info of notificationInformations) {
            noty({
                text: info,
                type: 'information'
            });
        }
    }
    if(typeof notificationSuccesses !== 'undefined') {
        for(let success of notificationSuccesses) {
            noty({
                text: success,
                type: 'success'
            });
        }
    }

});
