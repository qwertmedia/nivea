'use strict';

import 'bootstrap-fileinput/sass/fileinput';

import fileInput from 'bootstrap-fileinput/js/fileinput';

var imageSettingsPreview = $.extend({
    language: 'ru',
    uploadAsync: true,
    maxFileSize: 5000,
    ajaxSettings: {
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    },
    ajaxDeleteSettings: {
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    },
    allowedFileTypes: ['image'],

    overwriteInitial: false,
    browseClass: "btn btn-primary btn-block",
    showCaption: false,
    showRemove: false,
    showUpload: false

},  (typeof photo !== 'undefined') ? photo : {});
$('.image-upload').fileinput(imageSettingsPreview);
