'use strict';

import 'bootstrap-multiselect/dist/js/bootstrap-multiselect';
import 'bootstrap-multiselect/dist/css/bootstrap-multiselect';

$('.multiple-select').multiselect({
    enableFiltering: true,
    includeSelectAllOption: true
})
