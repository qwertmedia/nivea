'use strict';

import React from 'react';
import {Link} from 'react-router';
import {encode} from '../../link';

export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {loggedUser} = this.props;
        if(!loggedUser.teamAsCaptain) return null;

        return (
            <div>
                <p className="boldtext1 halfwidth sm-fullwidth">ВІТАЄМО!<br/> ВАША ЗАЯВКА ЗАРЕЄСТРОВАНА</p>
                <p className="bigtext halfwidth sm-fullwidth">Для остаточного оформлення заявки на участь, необхідно укомплектувати команду. Після 31 серпня Вам буде надіслано підтверждення та розклад ігор і Ваша команда буде зарахована до списку учасників турніру.</p>
                <Link to={`/app/team/${encode(loggedUser.teamAsCaptain.id)}`} className="btn">Карта команди</Link>
            </div>
        );
    }
}
