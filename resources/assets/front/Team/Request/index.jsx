'use strict';

import React from 'react';
import {Link} from 'react-router';
import { hashHistory } from 'react-router';

import Form from '../Form';

import { newTeamRequest } from '../../auth';

export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    submitNewRequest(data) {
        return newTeamRequest(data)
            .then(() => {
                hashHistory.push('/success');

                // tracking
                if (typeof Omniture !== 'undefined') Omniture.trackNOPV('request_reg_done');
            });
    }

    render() {
        return (
            <div>
                <p className="boldtext1">ПОДАЧА ЗАЯВКИ<br/>НА УЧАСТЬ У ТУРНІРІ</p>
                <p className="bigtext halfwidth sm-fullwidth">Заведіть обліковий запис команди та укомплектуйте її гравцями.</p>
                <Form className="w85p sm-fullwidth"
                    onSubmit={::this.submitNewRequest}
                    isFull={true}
                />
            </div>
        );
    }
}
