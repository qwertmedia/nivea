'use strict';

import React from 'react';

import request from '../request';
import { decode, encode } from '../link';
import Player from './Player';
import Form from './Form';

import appHistory from '../history';
import captainImg from '../img/ico_profile_cap.svg';
import teamImg from '../img/team_avatar.svg';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            captain: {
                firstName: '',
                lastName: '',
            },
            players: []
        };
    }

    componentDidMount() {
        const { link } = this.props.params;

        request('/api/v1/teams/' + decode(link), {}, 'GET')
            .then((team) => {
                this.setState(team);
                this.props.updateIframe();
            });
    }

    renderPlayers() {
        // don't show captain in players list
        let players = this.state.players.filter(player => player.id != this.state.captain.id);

        players = players.map((player, i) => {
            return <Player key={i} {...player} canEdit={false}/>
        });

        for(let i = players.length; i < 14; i++) {
            players.push(<Player key={i} canEdit={false}/>)
        }

        return (
            <ol className="players-list" id="player-list">
                {players}
            </ol>
        );
    }

    submitNewRequest(data) {
        return request('/api/v1/teams/' + this.state.id + '/players', data, 'POST')
            .then((player) => {
                let players = this.state.players;
                players.push(player);
                this.setState({players: players});
                appHistory.push(`/team/${this.props.params.link}/success`);

                // tracking
                if (typeof Omniture !== 'undefined') Omniture.trackNOPV(`ask-${this.state.name}-team_reg_done`);
            });
    }

    render() {
        const { loggedUser } = this.props;

        return (
            <div className="team-wrapper">
                <div className="gray-wrapper cf">
                    <div className="pull-left half-width sm-pull-none sm-fullwidth">
                        <h2 className="page-sub-title">ЗАЯВОЧНИЙ ЛИСТ КОМАНДИ</h2>
                        <div className="cf">
                            <img src={teamImg} width="110" className="team-logo pull-left" />
                            <p className="boldtext1 pull-left">{this.state.name}</p>
                        </div>
                    </div>
                    <div className="pull-right half-width sm-pull-none sm-fullwidth">
                        <h2 className="page-sub-title">ВАЖЛИВО</h2>
                        <p className="bigtext">Мінімальна кількість гравців однієї команди в заявочному листі - 10.<br/>
                            У заявці команди може бути до 15 осіб.<br/>
                            Щоб долучитися до команди, заповніть форму запиту капітану.</p>
                    </div>
                </div>

                <div className="cf">
                    <div className="halfwidth pull-left sm-fullwidth sm-pull-none">
                        <h2 className="page-sub-title">КАПІТАН КОМАНДИ</h2>
                        <div className="cf mb40">
                            <img src={captainImg} width="40" className="pull-left mr20" />
                            <p><strong>{this.state.captain.firstName} {this.state.captain.lastName}</strong></p>
                        </div>

                        <h2 className="page-sub-title">СКЛАД КОМАНДИ</h2>
                        <div>
                            <div className="player-list-wrapper">
                                {::this.renderPlayers()}
                            </div>
                        </div>
                    </div>

                    {/*(loggedUser.id && loggedUser.teamAsCaptain) ? null : (
                        <Form className="teamcard-user-form halfwidth pull-right sm-fullwidth sm-pull-none"
                            onSubmit={::this.submitNewRequest}
                            isFull={false}
                            extraInputClass={'fullwidth'}
                            extraLabelClass={'fullwidth'}
                        />
                    )*/}
                </div>
            </div>
        );
    }
}
