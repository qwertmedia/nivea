'use strict';

import React from 'react';

import request from '../request';
import { decode, encode } from '../link';
import Player from './Player';

import captainImg from '../img/ico_profile_cap.svg';
import teamImg from '../img/team_avatar.svg';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            captain: {
                firstName: '',
                lastName: '',
                id: 0
            },
            players: [],
            maxPlayers: 14,
            linkCopied: true,
            notification: ''
        };
    }

    componentDidMount() {
        const { link } = this.props.params;
        const linkData = decode(link);

        request('/api/v1/teams/' + linkData, {}, 'GET')
            .then((team) => {
                this.setState(team);
                this.props.updateIframe();
            });
    }

    hideAllPopups(id) {
        let players = this.state.players.map((player, i) => {
            player.popup = ((i != id) ? false : true);
            player.removeConfirm = false;
            return player;
        });
        this.setState({players})
    }

    renderPlayers() {
        // don't show captain in players list
        let players = this.state.players.filter(player => player.id !=this.state.captain.id);

        players = players.map((player, i) => {
            return <Player
                key={i} {...player}
                canEdit={true}
                removePlayer={this.removePlayer.bind(this, i)}
                hideAllPopups={this.hideAllPopups.bind(this, i)}
                updatePlayer={this.updatePlayer.bind(this, i)}
                acceptPlayer={this.acceptPlayer.bind(this, i)}
                showNotification={this.showNotification.bind(this)}
            />
        });
        for(let i = players.length; i < 14; i++) {
            players.push(<Player key={i} canEdit={false}/>)
        }
        return (
            <ol className="players-list" id="player-list">
                {players}
            </ol>
        );
    }

    removePlayer(i) {
        let players = this.state.players;
        players.splice(i, 1);
        this.setState({players});
    }

    copyLink(e) {
        e.preventDefault();
        this.refs.link.select();
        setTimeout(() => {
            this.setState({linkCopied: true})
        }, 5000);
        this.setState({linkCopied: false});
        document.execCommand('copy');
    }

    submitNewRequest(data) {
        request('/api/v1/teams/' + this.state.id + '/players', data, 'POST')
            .then((player) => {
                let players = this.state.players;
                players.push(player);
                this.setState({players: players});
            });
    }

    updatePlayer(i, data) {
        request('/api/v1/teams/' + data.team + '/players/' + data.id, data, 'POST')
            .then((player) => {
                let players = this.state.players;
                players[i] = player;
                this.setState({players});
            });
    }

    acceptPlayer(i, data) {
        request('/api/v1/teams/' + data.team + '/players/' + data.id + '/accept', {}, 'POST')
            .then((player) => {
                let players = this.state.players;
                players[i] = player;
                this.setState({players});
            });
    }


    showNotification(message) {
        if(this.timeout) clearTimeout(this.timeout);

        this.setState({notification: message});

        this.timeout = setTimeout(() => {
            this.setState({notification: ''});
        }, 5000);
    }

    render() {
        return (
            <div className="team-wrapper">
                <div className="gray-wrapper cf">
                    <div className="pull-left half-width sm-pull-none sm-fullwidth">
                        <h2 className="page-sub-title">ЗАЯВОЧНИЙ ЛИСТ КОМАНДИ</h2>
                        <div className="cf">
                            <img src={teamImg} width="110" className="team-logo pull-left" />
                            <p className="boldtext1 pull-left">{this.state.name}</p>
                        </div>
                    </div>
                    <div className="pull-right half-width sm-pull-none sm-fullwidth">
                        <h2 className="page-sub-title">ВАЖЛИВІ ПОДІЇ</h2>
                        <p>Після прийому заявок Оргкомітетом буде складено розклад, про що гравців повідомлять зазделегідь.</p>
                    </div>
                </div>

                <div className="halfwidth sm-fullwidth cf pb350">
                    <h2 className="page-sub-title">ВИ УВІЙШЛИ ЯК КАПІТАН КОМАНДИ</h2>
                    <div className="cf mb40">
                        <img src={captainImg} width="40" className="pull-left mr20" />
                        <p><strong>{this.state.captain.firstName} {this.state.captain.lastName}</strong></p>
                    </div>

                    <h2 className="page-sub-title">СКЛАД КОМАНДИ</h2>

                    {this.state.notification ? (
                        <p>{this.state.notification}</p>
                    ) : null}

                    {!this.state.players.length ? (
                        <div>
                            <p>У команді ще немає гравців.<br/>
                            Обов'язково запросіть у команду своїх друзів за допомогою посилання на картку команди. Тільки повністю укомплектовані команди будуть допущені до участі в турнірі.</p>
                            <input type="button" value="Копіювати посилання на команду" className="btn" onClick={::this.copyLink} />&nbsp;
                            {!this.state.linkCopied ? (
                                <span className="smalltext">посилання в буфері обміну</span>
                            ) : null}
                        </div>
                    ): null}

                    <div className="player-list-wrapper">
                        {::this.renderPlayers()}
                    </div>

                    {(this.state.players.length == this.state.maxPlayers) ? (
                        <p>Вільні місця у заявці команди відсутні. Команда укомплектована, заявка  прийнята до розгляду. Якщо Ви хочете замінити чи додати нового гравця, видаліть одного з учасників.</p>
                    ) : (this.state.players.length >= 10) ? (
                        <p>Команда укомплектована, заявка прийнята до розгляду, але у Вас є можливість додати ще гравців.</p>
                    ) : (
                        <p>Заявка не укомплектована цілком і не може бути прийнята до розгляду.</p>
                    )}

                    <br/>

                    <div>
                        <input type="button" value="Копіювати посилання на команду" className="btn" onClick={::this.copyLink} />&nbsp;
                        {!this.state.linkCopied ? (<span className="smalltext">посилання в буфері обміну</span>) : null}
                        <p>Щоб запросити гравця в команду, скопіюйте посилання на картку Вашої команди та відправте її.</p>
                    </div>

                    {this.state.id ? (<input type="text" ref="link" className="hidden-link" defaultValue={`${window.location.href.split("#")[0]}#/team/${encode(this.state.id)}`} />) : null}
                </div>
            </div>
        );
    }
}
