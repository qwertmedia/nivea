'use strict';

import React from 'react';
import {Link} from 'react-router';

export default class extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <div>
                <p className="boldtext1">ВІТАЄМО!<br/> ВАША ЗАЯВКА<br/> ЗАРЕЄСТРОВАНА</p>
                <p className="bigtext halfwidth">Тількі командна заявка приймається до розгляду,
                тому перший крок це завести облікові записи команди, а вже по ходу справи наповнити її гравцями.</p>
                <Link to={`/team/${this.props.params.link}`} className="btn">Карта команди</Link>
            </div>
        );
    }
}
