'use strict';

import React from 'react';
import {Link} from 'react-router';
import ReactPaginate from 'react-paginate';

import request from '../../request';
import { encode } from '../../link';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            teams: [],
            pageNum: 0,
            selected: 1,
            loading: false,
            originTeams: []
        }
    }

    componentDidMount() {
        this.loadTeams()
        .then(() => this.props.updateIframe());
    }

    loadTeams() {
        this.setState({loading: true});

        return request(`/api/v1/teams?page=${this.state.selected}`, {}, 'GET')
            .then(resp => {
                this.setState({
                    teams: resp.data,
                    originTeams: resp.data,
                    pageNum: resp.lastPage,
                    loading: false,
                    search: false
                });
            });
    }

    handlePageClick = data => {
        let { selected } = data;
        selected++;

        this.setState({ selected }, () => this.loadTeams());
    };

    onChange = (e)  => {
        let { value } = e.target;
        (value.length == 0) ? this.setState({search: false}) : this.setState({search: true});

        this.searchTeam(value);
    };

    searchTeam = this.debounce((value) => {
        request(`/api/v1/teams/search/${value}`, {}, 'GET')
                .then(teams => {
                    (this.state.search) ? this.setState({ teams }) : this.setState({teams: this.state.originTeams});
                });
    }, 500);

    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    render() {
        let { teams } = this.state;

        return (
            <div>
                <p className="boldtext1">КОМАНДИ</p>
                <p className="bigtext">Пошук</p>
                <p>
                    <input type="text" className='newfield' onChange={this.onChange} />
                </p>
                <br/>
                <p className="bigtext">Загальний список команд</p>

                <table className="teamlist" width="100%">
                    <thead>
                        <tr>
                            <th width="50%">Назва</th>
                            <th width="30%">Капітан</th>
                            <th>Гравці в команді</th>
                        </tr>
                    </thead>
                    <tbody>
                        {teams.map((team, i) => (
                            <tr key={i}>
                                <td>
                                    {team.photo
                                        ? (<img src={`/images/small/${team.photo.path}${team.photo.name}`}/>)
                                        : null}
                                    <Link to={`/team/${encode(team.id)}`} >{team.name}</Link>
                                </td>
                                <td>{team.captain ? team.captain.firstName : null} {team.captain ? team.captain.lastName : null}</td>
                                <td>{team.players.length}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>

                <ReactPaginate previousLabel={"назад"}
                       nextLabel={"вперед"}
                       breakLabel={<a>...</a>}
                       breakClassName={"break-me"}
                       pageNum={this.state.pageNum}
                       marginPagesDisplayed={2}
                       pageRangeDisplayed={5}
                       clickCallback={this.handlePageClick}
                       containerClassName={"pagination"}
                       subContainerClassName={"pages pagination"}
                       activeClassName={"active"} />

                {this.state.loading ? 'loading...' : ''}
            </div>
        );
    }
}
