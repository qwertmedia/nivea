'use strict';

import React from 'react';
import {Link} from 'react-router';
import {isNull, equals, isEmail} from 'validator';
import classNames from 'classnames';
import {toPattern} from 'vanilla-masker';
import Dropkick from 'dropkick/lib/dropkick';

import { parse } from '../request';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.initialState = {
            name: '',
            firstName: '',
            lastName: '',
            surname: '',
            year: 1986,
            month: 1,
            day: 1,
            phone: '',
            email: '',
            password: '',
            passwordConfirmation: '',
            about: '',
            rules: true,
            errors: []
        };
        this.state = Object.assign({}, this.initialState);
    }

    componentDidMount() {
        new Dropkick("#select-month", {
            mobile: true
        });
        new Dropkick("#select-day", {
            mobile: true
        });
        new Dropkick("#select-year", {
            mobile: true
        });
    }

    _collectInputValues() {
        let values = {};
        for (let input in this.refs) {
            values[input] = this.refs[input].value;
        }
        return values;
    }

    _handleChange(key, next) {
        let nextState = {};
        nextState[key] = next;
        this.setState(nextState);
    }

    fieldChange(name, e) {
        this._handleChange(name, e.target.value);
    }

    rulesChange(e) {
        this._handleChange('rules', !this.state.rules);
    }

    phoneChange(e) {
        this._handleChange('phone', e.target.value.replace(/(\+380|\s)/, ''));
    }

    _validateStringField(name, value) {
        this._toggleError(name, isNull(value));
    }

    validateStringField(name, e) {
        this._validateStringField(name, e.target.value);
    }

    _validatePhone(value) {
        this._toggleError('phone', !/^\+380\s[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/i.test(value))
    }

    validatePhone(e) {
        this._validatePhone(e.target.value);
    }

    _validateEmail(value) {
        this._toggleError('email', (!isEmail(value) || isNull(value)));
    }

    validateEmail(e) {
        this._validateEmail(e.target.value);
    }

    _validatePassword(value) {
        let password = this.refs.password.value;
        this._toggleError('passwordConfirmation', (!equals(password, value) || isNull(value)));
    }

    validatePassword(e) {
        this._validatePassword(e.target.value);
    }

    _validate(inputs) {
        let simple =  ['name', 'firstName', 'lastName', 'surname', 'day', 'month', 'year'];
        if(this.props.isFull) {
            simple = simple.concat(['password'])
            this._validatePassword(inputs['passwordConfirmation']);
        }
        for(let input in inputs) {
            if(~simple.indexOf(input)) {
                this._validateStringField(input, inputs[input])
            }
        }
        this._validateEmail(inputs['email']);
        this._validatePhone(inputs['phone']);
    }

    _toggleError(feild, error) {
        let errors = this.state.errors;
        if (error) {
            if (!~ errors.indexOf(feild)) {
                errors.push(feild);
            }
        } else {
            let index = errors.indexOf(feild);
            if (~ index)
                errors.splice(index, 1);
        }
        this._handleChange('errors', errors);
    }

    onSubmit(e) {
        e.preventDefault();
        let inputs = this._collectInputValues();
        this._validate(inputs);
        if (!this.state.errors.length && this.state.rules) {
            this.props.onSubmit(this.state)
                .then(() => {
                    this.setState(this.initialState);
                })
                .catch(err => {
                    if(err.response && err.response.status == 422) {
                        let errors = parse(err.response.data);
                        this.setState({errors: Object.keys(errors)});
                    }
                });
        }
    }

    renderBirthdayDate() {
        let date = [];
        for(let i = 1; i <= 31; i++) {
            date.push(<option key={i} value={i}>{i}</option>);
        }

        let monthes = {1: 'Січ', 2: 'Лют', 3: 'Бер', 4: 'Кві', 5: 'Тра', 6: 'Чер', 7: 'Лип', 8: 'Сер', 9: 'Вер', 10: 'Жов', 11: 'Лис', 12: 'Гру'};
        let month = [];
        for(let m in monthes) {
            month.push(<option key={m} value={m}>{monthes[m]}</option>);
        }

        let year = [];
        for(let i = 1960; i <= 1998; i++) {
            year.push(<option  key={i} value={i}>{i}</option>);
        }

        return (
            <div className={classNames('date-block', {w60p: this.props.isFull, 'sm-fullwidth': this.props.isFull, fullwidth: !this.props.isFull})}>
                <select value={this.state.day} onChange={this.fieldChange.bind(this, 'day')} id="select-month" ref="day">
                    {date}
                </select>
                <select value={this.state.month} onChange={this.fieldChange.bind(this, 'month')} id="select-day" ref="month">
                    {month}
                </select>
                <select value={this.state.year} onChange={this.fieldChange.bind(this, 'year')} id="select-year" ref="year">
                    {year}
                </select>
            </div>
        );
    }

    render() {
        return (
            <form onSubmit={:: this.onSubmit} className={this.props.className}>

                {this.props.isFull ? (
                    <div>
                        <p>
                            <label className="label">Назва команди</label>
                            <input type="text" className={classNames('newfield', this.props.extraInputClass, {
                                'has-error': ! !~ this.state.errors.indexOf('name')
                            })} value={this.state.name} onChange={this.fieldChange.bind(this, 'name')} onBlur={this.validateStringField.bind(this, 'name')} ref="name" maxLength="24"/>
                        </p>
                        <h2 className="page-sub-title">КАПІТАН КОМАНДИ</h2>
                    </div>
                ) : (
                    <div>
                        <h2 className="page-sub-title">ЗАПИТ КАПІТАНУ</h2>
                        <p>Бажаєте потрапити до цієї команди? Відправте запит капітану. Після того, як Ваш запит буде прийнято, ми надішлемо подтвердження про прийняття до складу команди Вам на E-mail.</p>
                    </div>
                )}

                <p>
                    <label className="label">Ім&#39;я</label>
                    <input type="text" className={classNames('newfield', this.props.extraInputClass, {
                        'has-error': ! !~ this.state.errors.indexOf('firstName')
                    })} value={this.state.firstName} onChange={this.fieldChange.bind(this, 'firstName')} onBlur={this.validateStringField.bind(this, 'firstName')} ref="firstName"/>
                </p>
                <p>
                    <label className="label">Прізвище</label>
                    <input type="text" className={classNames('newfield', this.props.extraInputClass, {
                        'has-error': ! !~ this.state.errors.indexOf('lastName')
                    })} value={this.state.lastName} onChange={this.fieldChange.bind(this, 'lastName')} onBlur={this.validateStringField.bind(this, 'lastName')} ref="lastName"/>
                </p>
                <p>
                    <label className="label">По-батькові</label>
                    <input type="text" className={classNames('newfield', this.props.extraInputClass, {
                        'has-error': ! !~ this.state.errors.indexOf('surname')
                    })} value={this.state.surname} onChange={this.fieldChange.bind(this, 'surname')} onBlur={this.validateStringField.bind(this, 'surname')} ref="surname"/>
                </p>
                <div>
                    <label className={classNames('label', this.props.extraLabelClass)}>Дата народження{this.props.isFull ? (<br/>) : null} День / Місяць / Рік</label>
                    {this.renderBirthdayDate()}
                </div>
                <p>
                    <label className="label">Номер телефону</label>
                    <input type="text" className={classNames('newfield', this.props.extraInputClass, {
                        'has-error': ! !~ this.state.errors.indexOf('phone')
                    })} value={this.state.phone ? toPattern(this.state.phone, '+380 99 999 99 99') : '+380'} onChange={:: this.phoneChange} onBlur={::this.validatePhone} ref="phone"/>
                </p>
                <p>
                    <label className="label">E-mail</label>
                    <input type="email" className={classNames('newfield', this.props.extraInputClass, {
                        'has-error': ! !~ this.state.errors.indexOf('email')
                    })} value={this.state.email} onChange={this.fieldChange.bind(this, 'email')} onBlur={::this.validateEmail} ref="email"/>
                </p>

                {this.props.isFull ? (
                    <p>
                        <label className="label">Пароль</label>
                        <input type="password" className={classNames('newfield', this.props.extraInputClass, {
                            'has-error': ! !~ this.state.errors.indexOf('password')
                        })} value={this.state.password} onChange={this.fieldChange.bind(this, 'password')} onBlur={this.validateStringField.bind(this, 'password')} ref="password"/>
                    </p>
                ) : null}

                {this.props.isFull ? (
                    <p>
                        <label className="label">Підтвердження пароля</label>
                        <input type="password" className={classNames('newfield', this.props.extraInputClass, {
                            'has-error': ! !~ this.state.errors.indexOf('passwordConfirmation')
                        })} value={this.state.passwordConfirmation} onChange={this.fieldChange.bind(this, 'passwordConfirmation')} onBlur={::this.validatePassword} ref="passwordConfirmation"/>
                    </p>
                ) : null}

                {!this.props.isFull ? (
                    <p>
    					<label>Коротко про себе, вік, досвід гри</label>
    					<textarea className={classNames('newfield', this.props.extraInputClass)} value={this.state.about} cols="30" rows="2" onChange={this.fieldChange.bind(this, 'about')} ref="about"></textarea>
    				</p>
                ) : null}

                <p>
                    {this.props.isFull ? (<label className="label"></label>): null }
                    <input id="agree" type="checkbox" value={this.state.rules} checked={this.state.rules} onChange={::this.rulesChange} ref="rules" required />
    				<label className="agree" htmlFor="agree"> Я приймаю <a href="http://www.niveamen.ua/play/pravyla" className="show_rules">умови використання</a></label>
    			</p>

                {!this.props.isFull ? (
                    <p>Капітан залишає за собою право прийняти або відхилити Вашу заявку без пояснення причин.</p>
                ): null }

                <p>
                    <label className="label"></label>
                    <input type="submit" value="Відправити запит" className="btn"/>
                </p>
            </form>
        );
    }
}
