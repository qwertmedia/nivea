'use strict';

import React from 'react';
import { Link } from 'react-router';
import { hashHistory } from 'react-router';
import {isNull, equals, isEmail} from 'validator';
import classNames from 'classnames';
import {toPattern} from 'vanilla-masker';

import { encode } from '../link';
import request from '../request';

import img from '../img/ico_profile.svg';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            popup: false,
            removeConfirm: false,
            phone: '',
            email: '',
            errors: [],
            inTeam: false,
            birthday: '0000-00-00',
            id: 0,
            team: 0,
            about: '',
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            phone: props.phone || '',
            email: props.email || '',
            popup: props.popup || false,
            removeConfirm: props.removeConfirm || false,
            id: props.id || 0,
            team: props.team ? props.team.id : 0,
            inTeam: props.inTeam || false,
            birthday: props.birthday || '0000-00-00',
            about: props.about || ''
        });
    }

    _handleChange(key, next) {
        let nextState = {};
        nextState[key] = next;
        this.setState(nextState);
    }

    emailChange(e) {
        this._handleChange('email', e.target.value);
    }

    phoneChange(e) {
        this._handleChange('phone', e.target.value.replace(/(\+380|\s)/, ''));
    }

    _validatePhone(value) {
        this._toggleError('phone', !/^\+380\s[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/i.test(value))
    }

    validatePhone(e) {
        this._validatePhone(e.target.value);
    }

    _validateEmail(value) {
        this._toggleError('email', (!isEmail(value) || isNull(value)));
    }

    validateEmail(e) {
        this._validateEmail(e.target.value);
    }

    _validate() {
        this._validateEmail(this.refs.email.value);
        this._validatePhone(this.refs.phone.value);
    }

    _toggleError(feild, error) {
        let errors = this.state.errors;
        if (error) {
            if (!~ errors.indexOf(feild)) {
                errors.push(feild);
            }
        } else {
            let index = errors.indexOf(feild);
            if (~ index)
                errors.splice(index, 1);
        }
        this._handleChange('errors', errors);
    }

    showPopup() {
        if(this.props.canEdit && !this.state.popup) {
            this.props.hideAllPopups(this.id);
        }
        if(this.state.popup) {
            this.setState({popup: false});
            this.setState({removeConfirm: false});
        }
    }

    onSubmit(e) {
        e.preventDefault();
        e.stopPropagation();
        this._validate();
        if (!this.state.errors.length) {
            this.props.updatePlayer.call(null, this.state);
            this.props.showNotification('Дані гравця збережено!');
        }
    }

    preventPropagation(e) {
        e.stopPropagation();
    }

    removeFromTeam(e) {
        e.preventDefault();
        e.stopPropagation();

        if(this.state.removeConfirm) {
            request(('/api/v1/teams/' + this.state.team + '/players/' + this.state.id), {}, 'delete')
                .then(() => {
                    this.setState({popup: false});
                    this.props.removePlayer();
                    this.props.showNotification('Гравець вилучений з команди!');
                });
        } else {
            this.setState({removeConfirm: true});
        }
    }

    acceptRequest(e) {
        e.preventDefault();
        e.stopPropagation();
        this.props.acceptPlayer.call(null, this.state);
        this.props.showNotification('Запит прийнято і повідомлення відправлено гравцеві. Про всяк випадок скопіюйте посилання на картку команди та відправте гравцеві.')
    }

    declineRequest(e) {
        e.preventDefault();
        e.stopPropagation();
        request((`/api/v1/teams/${this.props.pivot.teamId}/players/${this.props.pivot.userId}/decline`), {}, 'post')
            .then((user) => {
                this.setState({popup: false});
                this.props.removePlayer();
            });
    }

    render() {
        let date = this.state.birthday;

        date = date.split('-');
        return (
            <li onClick={::this.showPopup} className={classNames({'request': !this.state.inTeam},  {'active': this.state.popup})}>
                <img src={img} width="40" /><span>{this.props.firstName} {this.props.lastName}</span>

                {this.state.popup && this.props.inTeam ? (
                    <div className="edit-player active">

                        {this.state.removeConfirm ? (
                            <div>
                                <p>remove confirmation</p>
                                <input type="button" value="remove" onClick={::this.removeFromTeam} className="btn mr20"/>
                            </div>
                        ) : (
                            <div>
                                <p className="page-sub-title">ДАНІ ГРАВЦЯ</p>
                                <p>
                                    Ім'я<br/>
                                    {this.props.firstName}
                                </p>
                                <p>
                                    Прізвище<br/>
                                    {this.props.lastName}
                                </p>
                                <p>
                                    По-батькові<br/>
                                    {this.props.surname}
                                </p>
                                <form onSubmit={::this.onSubmit}>
                                    <p>
                                        <label>Дата народження</label> <br />
                                        {date[2]} / {date[1]} / {date[0]}
                                    </p>
                                    <p>
                                        <label className="label fullwidth">Номер телефону</label>
                                        <input type="text" className={classNames('newfield', 'fullwidth', {
                                            'has-error': ! !~ this.state.errors.indexOf('phone')
                                        })} value={toPattern(this.state.phone, '+380 99 999 99 99')} onChange={:: this.phoneChange} onClick={::this.preventPropagation} onBlur={::this.validatePhone} ref="phone"/>
                                    </p>
                                    <p>
                                        <label className="label">e-mail</label>
                                        <input type="email" className={classNames('newfield', 'fullwidth', {
                                            'has-error': ! !~ this.state.errors.indexOf('email')
                                        })} value={this.state.email} onChange={::this.emailChange} onBlur={::this.validateEmail} onClick={::this.preventPropagation} ref="email"/>
                                    </p>
                                    <p>
                                        <input type="submit" value="Зберегти" onClick={::this.preventPropagation} className="btn mr20"/>
                                        <a className="show_rules" onClick={::this.removeFromTeam}>Вилучити з команди</a>
                                    </p>
                                </form>
                            </div>
                        )}

                        <button className="close-popup">x</button>
                    </div>
                ) : null}

                {this.state.popup && !this.props.inTeam ? (
                    <div className="player-request active">
                        <p className="page-sub-title">ЗАПИТ ВІД ГРАВЦЯ</p>
                        <p>Потенційний гравець відправив запит для участі у турнірі у складі Вашої команди. Для внесення гравця в заявочний лист Вашої команди, необхідно Ваше підтвердження.</p>
                        <p>
                            Ім'я<br/>
                            {this.props.firstName}
                        </p>
                        <p>
                            Прізвище<br/>
                            {this.props.lastName}
                        </p>
                        <p>
                            По-батькові<br/>
                            {this.props.surname}
                        </p>
                        <p>
                            Дата народження<br/>
                            {date[2]} / {date[1]} / {date[0]}
                        </p>
                        <p>
                            Номер телефону<br/>
                            {this.state.phone}
                        </p>
                        <p>
                            e-mail<br/>
                            {this.state.email}
                        </p>
                        <p>
                            Коротко про себе: вік, досвід гри<br/>
                            {this.state.about}
                        </p>
                        <p>
                            <a className="btn mr20" onClick={::this.acceptRequest}>Прийняти</a>
                            <a className="show_rules" onClick={::this.declineRequest}>Відхилити</a>
                        </p>
                        <button className="close-popup">x</button>
                    </div>
                ) : null}

            </li>
        )
    }
}
