'use strict';

import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';
import {reset} from '../../auth';

export default class extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: []
        };
    }

    onSubmit(e) {
        e.preventDefault();
        let email = this.refs.email.value;
        reset({email})
            .then(data => {
                history.replace('/reset/success');
            })
            .catch(err => {
                if(err.response && err.response.status == 422) {
                    this.setState({errors: Object.keys(err.response.data)});
                }
            });
    }

    render() {
        return (
            <div>
                <p className="boldtext1">НАГАДУВАННЯ ПАРОЛЯ</p>
                <p className="bigtext">Для нагадування вкажіть E-mail, за яким було здійснено реєстрацію.</p>
                <form className="w85p sm-fullwidth" onSubmit={::this.onSubmit}>
                    <p>
            			<label className="label">E-mail капітана команди</label>
            			<input type="email" className={classNames('newfield', {'has-error': ! !~ this.state.errors.indexOf('email')})} ref="email" required />
            		</p>
                    <p>
                        <label className="label"></label>
                        <span><Link to="/login" className="show_rules">Увійти</Link></span>
                    </p>
                    <br />
            		<p>
            			<label className="label"></label>
            			<input type="submit" value="Відправити" className="btn" />
            		</p>
            	</form>
            </div>
        );
    }
}
