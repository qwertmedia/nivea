'use strict';

import React from 'react';
import {Link} from 'react-router';

export default class extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <div>
                <p className="boldtext1 halfwidth sm-fullwidth">Дякуємо, на Вашу електронну адресу відправлено новий пароль.</p>
                <Link to="/login" className="btn">Форма входу</Link>
            </div>
        );
    }
}
