'use strict';

import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';
import {isNull, isEmail} from 'validator';
import { encode } from '../link';

import history from '../history';
import {login} from '../auth';


export default class extends React.Component {
    constructor(props) {
        super(props);
        this.initialState = {
            email: '',
            password: '',
            errors: []
        };
        this.state = Object.assign({}, this.initialState);
    }

    _collectInputValues() {
        let values = {};
        for (let input in this.refs) {
            values[input] = this.refs[input].value;
        }
        return values;
    }

    _handleChange(key, next) {
        let nextState = {};
        nextState[key] = next;
        this.setState(nextState);
    }

    fieldChange(name, e) {
        this._handleChange(name, e.target.value);
    }

    _validateStringField(name, value) {
        this._toggleError(name, isNull(value));
    }

    validateStringField(name, e) {
        this._validateStringField(name, e.target.value);
    }

    _validateEmail(value) {
        this._toggleError('email', (!isEmail(value) || isNull(value)));
    }

    validateEmail(e) {
        this._validateEmail(e.target.value);
    }

    _toggleError(feild, error) {
        let errors = this.state.errors;
        if (error) {
            if (!~ errors.indexOf(feild)) {
                errors.push(feild);
            }
        } else {
            let index = errors.indexOf(feild);
            if (~ index)
                errors.splice(index, 1);
        }
        this._handleChange('errors', errors);
    }

    _validate(inputs) {
        this._validateStringField('password', inputs.password)
        this._validateEmail(inputs.email);
    }

    onSubmit(e) {
        e.preventDefault();
        let inputs = this._collectInputValues();
        this._validate(inputs);

        if (!this.state.errors.length) {
            login(this.state)
                .then(user => {
                    if(user) {
                        history.replace(`/app/team/${encode(user.teamAsCaptain.id)}`)
                    }
                })
                .catch(err => {
                    if(err.response && err.response.status == 401) {
                        this.setState({errors: Object.keys(err.response.data)});
                    }
                });
        }
    }

    render() {
        return (
            <div>
                <p className="boldtext1">ВХІД В АККАУНТ КОМАНДИ</p>
                <form className="w85p sm-fullwidth" onSubmit={::this.onSubmit}>
            		<p>
            			<label className="label">E-mail<br/>капітана команди</label>
                        <input type="text" className={classNames('newfield', {
                            'has-error': ! !~ this.state.errors.indexOf('email')
                        })} value={this.state.email} onChange={this.fieldChange.bind(this, 'email')} onBlur={::this.validateEmail} ref="email"/>
            		</p>
            		<p>
            			<label className="label">Пароль</label>
                        <input type="password" className={classNames('newfield', {
                            'has-error': ! !~ this.state.errors.indexOf('password')
                        })} value={this.state.password} onChange={this.fieldChange.bind(this, 'password')} onBlur={this.validateStringField.bind(this, 'password')} ref="password"/>
            		</p>
            		<p>
            			<label className="label"></label>
            			<span><Link to="/reset" className="show_rules">Забув пароль?</Link></span>
            		</p>
            		<br />
            		<p>
            			<label className="label"></label>
            			<input type="submit" value="Увійти" className="btn" id="regcode" />
            		</p>
            	</form>
            </div>
        );
    }
}
