'use strict';

import request from './request';
import {decode} from './link';
import {setIsAuthed, getIsAuthed, checkAuth} from './auth';

let isAuthed = false;

export default (nextState, replace, callback) => {
    if(!getIsAuthed()) {
        checkAuth()
            .then(() => replace(nextState.location.pathname))
            .catch(() => replace('/login'));
    }
    callback();
}
