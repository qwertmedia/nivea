'use strict';

import Hashids from 'hashids';

const hashids = new Hashids('THIS IS SALT AAAAAAAAAAAAAAAAAAA', 11);

export function encode(...data) {
    return hashids.encode(data);
}
export function decode(data) {
    return hashids.decode(data);
}
