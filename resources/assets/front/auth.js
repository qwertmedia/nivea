'use strict';

import request from './request';
import {encode} from './link';
import history from './history';

let isAuthed = false;
let onChagneCallback = null;


export function setIsAuthed(authed) {
    isAuthed = authed;
}

export function getIsAuthed() {
    return isAuthed;
}

export function onChange(callback) {
    onChagneCallback = callback;
}

export function checkAuth() {
    return request('/api/v1/auth', {}, 'GET')
        .then(user => {
            if(user) {
                onChagneCallback(user);
            }
        })
        .catch (err => {
            console.log(err);
        });
}

export function login(data) {
    return request('/api/v1/auth/login', data, 'POST')
        .then((user) => {
            if(user && user.inTeam) {
                onChagneCallback(user);
                setIsAuthed(true);
                return user;
            } else {
                console.error('User in not captain');
            }
        });
}

export function logout() {
    return request('/api/v1/auth/logout', {}, 'GET')
        .then(() => {
            setIsAuthed(false);
            onChagneCallback({
                id: false,
                email: '',
                firstName: '',
                lastName: '',
                teamAsCaptain: {
                    id: 0,
                    name: ''
                }
            });
            history.replace('/');
        });
}

export function reset(data) {
    return request('/api/v1/auth/reset', data, 'POST');
}

export function newTeamRequest(data) {
    return request('/api/v1/teams', data, 'POST')
        .then((user) => {
            onChagneCallback(user);
            setIsAuthed(true);
        });
}
