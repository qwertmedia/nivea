'use strict';

import React from 'react';
import { Link } from 'react-router';
import appHistory from './history';

import { encode } from './link';
import request from './request';
import {onChange, checkAuth, logout} from './auth';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        // check autorization
        onChange(::this.updateAuth);
        checkAuth();

        // resize iframe
        this.updateIframe();
        if(window != top) window.parent.addEventListener('resize', this.updateIframe);


        // take hash from parent window if in iframe
        var outerHash = (window != top) ? window.top.location.hash : '';
        if(outerHash) location.hash = outerHash;
        outerHash = '';
    }

    componentDidUpdate() {
        // resize iframe
        this.updateIframe();

        // tracking
        if (typeof Omniture !== 'undefined') Omniture.trackSubPageView(this.props.location.pathname);
    }

    updateAuth(user) {
        this.setState(user);
    }

    updateIframe() {
        if(window == top) {return} // not in iframe

        var offsetHeight = document.documentElement.offsetHeight,
            desktopIframe = parent.document.getElementsByTagName('iframe')[0],
            mobileIframe = parent.document.getElementsByTagName('iframe')[1];

        if(getComputedStyle(desktopIframe).display != 'none') {
            desktopIframe.height = offsetHeight;
        } else if(mobileIframe && getComputedStyle(mobileIframe).display != 'none') {
            mobileIframe.height = offsetHeight;
        };
    }

    render() {
        return (
            <div>
                <div className="tabs">
                    <Link to="/teams" className="tabitem" activeClassName="active">Команди</Link>
                    {this.state.teamAsCaptain ? (
                        <Link to={"/app/team/" + encode(this.state.teamAsCaptain.id)} className="tabitem" activeClassName="active">Моя команда</Link>
                    ) : ''}

                    <div className="pull-right">
                        {this.state.id ? (
                            <span className="tabitem user active">{this.state.firstName} {this.state.lastName}</span>
                        ) : null}

                        {this.state.id ? (
                            <span className="tabitem" onClick={logout}>Вихід</span>
                        ) : (
                            <Link to="/login" className="tabitem" activeClassName="active">Вхід для капітанів</Link>
                        )}
                    </div>
                </div>

                {React.cloneElement(this.props.children, {
                    loggedUser: this.state,
                    updateIframe: this.updateIframe
                })}
            </div>
        )
    }
}
