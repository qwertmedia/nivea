'use strict';

import './style';

import React from 'react';
import { Link } from 'react-router';
import { hashHistory } from 'react-router';
import { encode } from './link';

import request from './request';

export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    // login(data) {
    //     request('/api/v1/auth/logout', {}, 'GET')
    //         .then((user) => {
    //             this.setState(user)
    //             
    //         });
    // }

    render() {
        //if(!this.state.teamAsCaptain.id) return null;
        return (
            <div>
                <div className="tabs">
                    <Link to="/teams" className="tabitem">Команди</Link>
                    <Link to="/" className="tabitem">Зібрати Команду</Link>
                    <div className="pull-right">
                        <Link to="/login" className="tabitem">Вхід для капітанів</Link>
                    </div>
                </div>
                {this.props.children}
            </div>
        )
    }
}
