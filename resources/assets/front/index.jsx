'use strict';

import 'dropkick/lib/css/dropkick.css';
import './pagination';
import './style';

import React from 'react';
import {render} from 'react-dom';
import {Router, Route, IndexRoute, useRouterHistory, IndexRedirect} from 'react-router';
import { createHashHistory } from 'history'
const appHistory = useRouterHistory(createHashHistory)({ queryKey: false })

import App from './App';
import Login from './Login';
import Reset from './Login/Reset';
import ResetSuccess from './Login/Reset/Success';

import TeamList from './Team/List';
import TeamRequest from './Team/Request';
import RequestSuccess from './Team/Request/Success';

import Team from './Team';
import TeamView from './Team/View';
import TeamSuccess from './Team/Success';

import barrier from './barrier';


render((
    <Router history={appHistory}>
        <Route path="/" component={App}>
            <IndexRedirect to="teams" />
            {/*<Route path="/request" component={TeamRequest}/>*/}
            <Route path="/success" component={RequestSuccess}/>
            <Route path="/teams" component={TeamList}/>
            <Route path="/login" component={Login}/>
            <Route path="/team/:link" component={TeamView}/>
            <Route path="/team/:link/success" component={TeamSuccess}/>
            <Route path="/app/team/:link" onEnter={barrier} component={Team}/>
            <Route path="/reset">
                <IndexRoute component={Reset}/>
                <Route path="success" component={ResetSuccess}/>
            </Route>
        </Route>
    </Router>
), document.getElementById('app'));
