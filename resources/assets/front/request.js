'use strict';

import axios from 'axios';

const ENV = ~location.host.indexOf('local') ? 'development' : 'production';

function generate(initial) {
    let data = {};
    for (let i in initial) {
        if(Object.prototype.toString.call(initial[i]) !== '[object Array]') {
            data[i.replace(/([A-Z])/g, ($1) => ("_" + $1.toLowerCase()))] = initial[i];
        }
    }
    return data;
}

export function parse(initial) {
    if(Object.prototype.toString.call(initial) === '[object Array]') {
        return initial.map(init => parse(init));
    }
    let data = {}
    for (let i in initial) {
        if(Object.prototype.toString.call(initial[i]) === '[object Array]') {
            data[i.replace(/(\_[a-z])/g, ($1) => ($1.toUpperCase().replace('_','')))] = initial[i].map(init => parse(init));
        } else if(Object.prototype.toString.call(initial[i]) === '[object Object]') {
            data[i.replace(/(\_[a-z])/g, ($1) => ($1.toUpperCase().replace('_','')))] = parse(initial[i]);
        } else if(initial[i]){
            data[i.replace(/(\_[a-z])/g, ($1) => ($1.toUpperCase().replace('_','')))] = initial[i];
        }
    }
    return data;
}

export default function(url, data, method = 'POST') {
    return axios({
        url: (((ENV == 'production') ? 'http://ext14.niveamen.ua/23/play_root/public' : '') + url),
        method: method,
        headers: {
            'X-CSRF-TOKEN': document.querySelector('meta[name="_token"]').getAttribute("content"),
            'X-Requested-With': 'XMLHttpRequest'
        },
        data: generate(data)
    }).then(response => {
        if(response.status == 422) { throw new Error(); }
        return parse(response.data);
    });
}
