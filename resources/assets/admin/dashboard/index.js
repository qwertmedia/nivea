'use strict';

import 'bootstrap/dist/css/bootstrap';
import 'font-awesome/css/font-awesome';
import './style';

import 'bootstrap/js/dropdown';
import '../../modules/notifier';
import '../../modules/table';
import '../../modules/image-upload';
import '../../modules/select';
