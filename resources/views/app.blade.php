<!DOCTYPE html>
<html>
<head>
  <title>Nivea</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="_token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="css/front.css?time={{time()}}" media="all" charset="utf-8">
  <script src="http://www.niveamen.ua/resrc/NIVEAMEN/javascripts/tracking_iframe.js"></script>
</head>
<body>
  <div class="container" id="app"></div>

  <script type="text/javascript" src="js/front.js?time={{time()}}"></script>
</body>
</body>
</html>
