<!DOCTYPE html>
<html>
    <head>
        <title>Nivea</title>
        @if (($status == 'success') || !object_get($team, 'id'))
            <script>
                window.top.location.href = "{{env('APP_URL')}}";
            </script>
        @endif
    </head>
    <body>
        Remove team?
        <a href="{{ route('remove-team', [
                'link' => object_get($team, 'link'),
                'approved' => true
            ]) }}">Yes</a>
        <a href="{{ env('APP_URL') }}">No</a>
    </body>
    </body>
</html>
