@extends('admin.index')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
@section('content')
    <div class="side-body">
        <div class="page-title">
            <span class="title">Schelude"</span>
        </div>
        <form action="{{ $schelude->id ? route('schelude', ['id' => $schelude->id]) : route('create-schelude') }}" method="post">
            <div class="row">
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Info</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="sub-title">Team 1</div>
                                    <div>
                                        <input type="text" class="form-control" placeholder="" name="team1_id" value="{{ object_get($schelude, 'team1_id', old('team1_id')) }}">
                                    </div>
                                  <div class="sub-title">Team 2</div>
                                    <div>
                                        <input type="text" class="form-control" placeholder="" name="team2_id" value="{{ object_get($schelude, 'team2_id', old('team2_id')) }}">
                                    </div>
                                    <div class="sub-title">Date</div>
                                    <div>
                                        <input type="text" class="form-control" placeholder="" name="date" value="{{ object_get($schelude, 'date', old('date')) }}">
                                    </div>
                                    <div class="sub-title">Score</div>
                                    <div>
                                        <input type="text" class="form-control" placeholder="" name="score" value="{{ object_get($schelude, 'score', old('score')) }}">
                                    </div>
                                    <div class="sub-title">Group Stage</div>
                                    <div>
                                        <input type="text" class="form-control" placeholder="" name="group_stage" value="{{ object_get($schelude, 'group_stage', old('group_stage')) }}">
                                    </div>
                                    <div>
                                        <input type="textarea" class="form-control" placeholder="" name="comment" value="{{ object_get($schelude, 'comment', old('comment')) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                </div>
                <div class="col-xs-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Save</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group">
                                            <input type="submit" class="btn btn-success" role="button" value="Save">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
@endsection
