@extends('admin.index')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
@section('content')
    <div class="side-body">
        <div class="page-title">
            <span class="title">Teams list</span>
            <a href="{{ route('create-team') }}" class="btn btn-primary pull-right">Create team</a>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">Table</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="datatable table table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Captain</th>
                                    <th>Player count</th>
                                    <th>Status</th>
                                    <th>Register Status</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                    <th>Name</th>
                                    <th>Captain</th>
                                    <th>Player count</th>
                                    <th>Status</th>
                                    <th>Register Status</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($teams as $team)
                                    <tr>
                                        <td>
                                            @if($team->photo)
                                                <img src="/images/small/{{ $team->photo->path }}{{ $team->photo->name }}" alt="{{ $team->photo->originalName }}" />
                                            @endif
                                        </td>
                                        <td>{{ $team->name }}</td>
                                        <td>
                                            @if($team->captain)
                                                {{ $team->captain->first_name }} {{ $team->captain->last_name }}
                                            @endif
                                        </td>
                                        <td>{{$team->players->count()}}</td>
                                        <td> 
                                            <div class="form-group">
                                              {{ Form::select('number', $statuses, $team->status) }}
                                            </div>
                                        </td>
                                        <td>
                                            {{ Form::select('number', $statuses, $team->register_status) }}
                                        </td>
                                        <td class="text-center"><a href="{{ route('team', [$team->id]) }}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                        <td class="text-center"><a href="{{ route('delete-team', [$team->id]) }}" onclick="return confirm('Удалить команду?');"><i class="glyphicon glyphicon-trash"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
$(".change-reg-status").change(function () {
        $.ajax({
            url:'/admin/teams/changeregisterstatus/'+this.id+'/'+this.value,
            method:'GET',
            dataType:'text',

        });
        console.log(this.id,this.value);
    });
 $(".change-status").change(function () {
        $.ajax({
            url:'/admin/teams/changestatus/'+this.id+'/'+this.value,
            method:'GET',
            dataType:'text',

        });
        console.log(this.id,this.value);
    });
</script>