@extends('admin.index')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
@section('content')
    <div class="side-body">
        <div class="page-title">
            <span class="title">Team "{{ $team->name }}"</span>
        </div>
        <form action="{{ $team->id ? route('team', ['id' => $team->id]) : route('create-team') }}" method="post">
            <div class="row">
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Info</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="sub-title">Name</div>
                                    <div>
                                        <input type="text" class="form-control" placeholder="Name" name="name" value="{{ object_get($team, 'name', old('name')) }}">
                                    </div>
                                    <div class="sub-title">Status</div>
                                    <div>
                                         <select name="status">
                                              <option value="1">Plays</option>
                                              <option value="0">Eliminated</option>
                                            </select> 
                                    </div>
                                    <div class="sub-title">Register Status</div>
                                    <div>
                                         <select name="register_status">
                                              <option value="1">Enable</option>
                                              <option value="0">Disable</option>
                                            </select> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Players</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="sub-title">Players</div>
                                            <div>
                                                <select class="form-control multiple-select" name="players[]" multiple="multiple">z
                                                    @foreach($users as $user)
                                                        @if(!$user->in_team || $team->players->contains($user->id))
                                                            <option {{ $team->players->contains($user->id) ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="sub-title">Captain</div>
                                            <div>
                                                <select class="form-control" name="captain_id">
                                                    <option value=""></option>
                                                    @foreach($users as $user)
                                                        @if(!$user->in_team || (object_get($team, 'captain') && $team->captain->id == $user->id))
                                                            <option {{ $user->id === object_get($team->captain, 'id') ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Save</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group">
                                            <input type="submit" class="btn btn-success" role="button" value="Save">
                                        </div>
                                        @if($team->id)
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{ route('delete-team', [$team->id]) }}">Delete</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($team->id)
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <div class="title">Image</div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <script>
                                        var photo = {!! ($team->photo ? json_encode($team->photo->getData()) : '{}') !!};
                                        </script>
                                        <input type="file" name="image" class="image-upload" data-upload-url="{{ route('photo-team-upload', ['team', $team->id]) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
@endsection
<script type="text/javascript">
$(document).ready(function(){
    var register_status='<?=$statuses["register_status"]?>';
    var status='<?=$statuses["status"]?>';
    $('[name=status]').val(status);
    $('[name=register_status]').val(register_status);
    console.log(status,register_status);
});
</script>