@extends('admin.index')

@section('content')
    <div class="side-body">
        <div class="page-title">
            <span class="title">Users list</span>
            <a href="{{ route('create-user') }}" class="btn btn-primary pull-right">Create user</a>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">Table</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="datatable table table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Roles</th>
                                    <th>In team as captain</th>
                                    <th>In team as player</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Roles</th>
                                    <th>In team as captain</th>
                                    <th>In team as player</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            @if($user->photo)
                                                <img src="/images/small/{{ $user->photo->path }}{{ $user->photo->name }}" alt="{{ $user->photo->originalName }}" />
                                            @endif
                                        </td>
                                        <td>{{ $user->name }}</td>
                                        <td>
                                            @foreach($user->roles as $role)
                                                <p>{{ $role->name }}</p>
                                            @endforeach
                                        </td>
                                        <td>
                                            @if($user->teamAsCaptain)
                                                <a href="{{ route('team', [$user->teamAsCaptain->id])}}">{{ $user->teamAsCaptain->name }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->team)
                                                <a href="{{ route('team', [$user->team->id])}}">{{ $user->team->name }}</a>
                                            @endif
                                        </td>
                                        <td class="text-center"><a href="{{ route('user', [$user->id]) }}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                        <td class="text-center">
                                            @if(!$user->is('superadmin'))
                                                <a href="{{ route('delete-user', [$user->id]) }}" onclick="return confirm('Удалить пользователя?');"><i class="glyphicon glyphicon-trash"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
