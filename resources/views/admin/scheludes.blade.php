@extends('admin.index')
<?php use App\Models\Team; ?>
@section('content')
    <div class="side-body">
        <div class="page-title">
            <span class="title">Scheludes list</span>
            <a href="{{ route('create-schelude') }}" class="btn btn-primary pull-right">Create schelude</a>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">Table</div>
                            <div><a href="{{ route('import-schelude') }}">Import Scheludes</a></div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="datatable table table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Team 1</th>
                                    <th>Team 2</th>
                                    <th>Date</th>
                                    <th>Score</th>
                                    <th>Group Stage</th>
                                    <th>Comment</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                   <th></th>
                                    <th>Team 1</th>
                                    <th>Team 2</th>
                                    <th>Date</th>
                                    <th>Score</th>
                                    <th>Group Stage</th>
                                    <th>Comment</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($scheludes as $schelude)
                                    <tr>
                                        <td></td>
                                        <td>
                                            {{ $schelude->team1_name}}
                                        </td>
                                        <td>{{ $schelude->team2_name }}</td>
                                        <td>
                                            {{$schelude->date}}
                                        </td>
                                        <td>{{$schelude->score}}</td>
                                        <td> 
                                            {{$schelude->group_stage}}
                                        </td>
                                        <td> 
                                            {{$schelude->comment}}
                                        </td>
                                        <td>
                                        </td>
                                        <td class="text-center"><a href="{{ route('schelude', [$schelude->id]) }}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                        <td class="text-center"><a href="{{ route('delete-schelude', [$schelude->id]) }}" onclick="return confirm('Удалить команду?');"><i class="glyphicon glyphicon-trash"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
