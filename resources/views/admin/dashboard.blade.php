@extends('admin.index')

@section('content')
    <div class="side-body padding-top">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a href="{{ route('users') }}">
                    <div class="card green summary-inline">
                        <div class="card-body">
                            <i class="icon fa fa-user fa-4x"></i>
                            <div class="content">
                                <div class="title">{{ $users->count() }}</div>
                                <div class="sub-title">Users</div>
                            </div>
                            <div class="clear-both"></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a href="{{ route('teams') }}">
                    <div class="card green summary-inline">
                        <div class="card-body">
                            <i class="icon fa fa-users fa-4x"></i>
                            <div class="content">
                                <div class="title">{{ $teams->count() }}</div>
                                <div class="sub-title">Teams</div>
                            </div>
                            <div class="clear-both"></div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <a href="{{ route('scheludes') }}">
                            <div class="card green summary-inline">
                                <div class="card-body">
                                    <i class="icon fa fa-users fa-4x"></i>
                                    <div class="content">
                                        <div class="title">{{ $scheludes->count() }}</div>
                                        <div class="sub-title">Scheludes</div>
                                    </div>
                                    <div class="clear-both"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
@endsection
