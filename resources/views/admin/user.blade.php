@extends('admin.index')

@section('content')
    <div class="side-body">
        <div class="page-title">
            <span class="title">User {{ $user->first_name }} {{ $user->last_name }}</span>
        </div>
        <form action="{{ $user->id ? route('user', ['id' => $user->id]) : route('create-user') }}" method="post">
            <div class="row">
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Info</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="sub-title">First name</div>
                                    <div>
                                        <input type="text" class="form-control" name="first_name" placeholder="First name" value="{{ object_get($user, 'first_name', old('first_name')) }}">
                                    </div>
                                    <div class="sub-title">Last name</div>
                                    <div>
                                        <input type="text" class="form-control" name="last_name" placeholder="Last name" value="{{ object_get($user, 'last_name', old('last_name')) }}">
                                    </div>
                                    <div class="sub-title">Surname</div>
                                    <div>
                                        <input type="text" class="form-control" name="surname" placeholder="Surname" value="{{ object_get($user, 'surname', old('surname')) }}">
                                    </div>
                                    <div class="sub-title">Phone</div>
                                    <div>
                                        <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ object_get($user, 'phone', old('phone')) }}">
                                    </div>
                                    <div class="sub-title">Email</div>
                                    <div>
                                        <input type="email" class="form-control" name="email" placeholder="Email" value="{{ object_get($user, 'email', old('email')) }}">
                                    </div>
                                    <div class="sub-title">Birthday d/m/y</div>
                                    <div class="row">
                                        <div class="col-xs-1">
                                            <select class="form-control" name="day">
                                                @for($i = 1; $i <= 31; $i++)
                                                    <option {{array_get($user->split_birthday, 2) == $i ? 'selected' : '' }} value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-xs-1">
                                            <select class="form-control" name="month">
                                                @for($i = 1; $i <= 12; $i++)
                                                    <option {{array_get($user->split_birthday, 1) == $i ? 'selected' : '' }} value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-xs-2">
                                            <select class="form-control" name="year">
                                                @for($i = 1950; $i <= 2016; $i++)
                                                    <option {{array_get($user->split_birthday, 0) == $i ? 'selected' : '' }} value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                      <div class="sub-title">Roles</div>
                                      <div>
                                          <select class="form-control multiple-select" name="roles[]" multiple="multiple">
                                              @foreach($roles as $role)
                                                  <option {{ object_get($user, 'roles', collect(old('roles', [])))->contains($role->id) ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->name }}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Save</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <div class="btn-group">
                                            <input type="submit" class="btn btn-success" role="button" value="Save">
                                        </div>
                                        @if(!$user->is('superadmin') && $user->id)
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{ route('delete-user', [$user->id]) }}">Delete</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Password</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="sub-title">Password</div>
                                    <div>
                                        <input type="password" class="form-control" name="password" placeholder="Password" value="">
                                    </div>
                                    <div class="sub-title">Password confirmation</div>
                                    <div>
                                        <input type="password" class="form-control" name="password_confirmation" placeholder="Password confirmation" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($user->id)
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <div class="title">Image</div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <script>
                                        var photo = {!! ($user->photo ? json_encode($user->photo->getData()) : '{}') !!};
                                        </script>
                                        <input type="file" name="image" class="image-upload" data-upload-url="{{ route('photo-user-upload', ['user', $user->id]) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
@endsection
