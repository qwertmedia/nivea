<li class="{{Request::is('admin') ? 'active' : '' }}">
    <a href="{{ route('dashboard') }}">
        <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
    </a>
</li>
<li class="{{Request::is('admin/users*') ? 'active' : '' }}">
    <a href="{{ route('users') }}">
        <span class="icon fa fa-user"></span><span class="title">Users</span>
    </a>
</li>
<li class="{{Request::is('admin/teams*') ? 'active' : '' }}">
    <a href="{{ route('teams') }}">
        <span class="icon fa fa-users"></span><span class="title">Teams</span>
    </a>
</li>
<li class="{{Request::is('admin/scheludes*') ? 'active' : '' }}">
    <a href="{{ route('scheludes') }}">
        <span class="icon fa fa-users"></span><span class="title">Scheludes</span>
    </a>
</li>
