<!DOCTYPE html>
<html>

<head>
    <title>Admin :: Auth</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/css/auth.css">
</head>

<body class="flat-blue login-page">
    <div class="container">
        <div class="login-box">
            <div>
                <div class="login-form row">
                    <div class="col-sm-12">
                        <div class="login-body">
                            <form method="post" action="{{ route('login') }}">
                                <div class="control">
                                    <input type="text" name="email" class="form-control" value="" placeholder="Email"/>
                                </div>
                                <div class="control">
                                    <input type="password" name="password" class="form-control" value="" placeholder="Password"/>
                                </div>
                                <div class="login-button text-center">
                                    <input type="submit" class="btn btn-primary" value="Login">
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('blocks.notifier')
    <script type="text/javascript" src="/js/auth.js"></script>
</body>

</html>
