<!DOCTYPE html>
<html>
    <head>
        <title>Nivea</title>

    </head>
    <body>
        @include('emails.coverletterimage')
        <h3>Ваша команда зареєстрована!</h3>
        <p>Вітаємо з успішною реєстрацією!</p>
        <p>
            Ваш логін: {{$user->first_name}}
            Пароль: {{$password}}
        </p>
        <p>
            Тепер Ви можете збирати гравців до своєї команди. Відправте <a href="{{$link}}">посилання на картку команди</a>  своїм друзям, щоб вони внесли свої імена до заявочного листа.    
        </p>
        <div id="button-wrapper" style="text-align: center;">
            <div id="button-block" style="display: inline-block;border: 1px solid #003566;background-color: #003566;border-radius: 1px;">
                <a href="{{$linkApp}}" style="text-decoration:none">
                    <span class="button-text" style="padding: 30px;color: white;font-size: 3em; display:block;">Картка команди</span>
                </a>
            </div>
        </div>         
        <p>Нагадуємо, що у Вашій команді має бути не менше 6 і не більше 15 гравців. Збирайте команду та готуйтеся до перемог!</p>
        <a href="NIVEAMEN.ua/play">Правила акції</a>
        <a href="NIVEAMEN.ua/play">Регламент турніру</a>
        <p><a href="{{$linkDecline}}">Також Ви можете відмовитися</a> від реєстрації команди.</p>

    </body>
    
</html>


