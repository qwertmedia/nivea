<!DOCTYPE html>
<html>
    <head>
        <title>Nivea</title>
    </head>
    <body>
    	@include('emails.coverletterimage')
        <p>Вы, {{$user->first_name}} {{$user->last_name}}, отправили запрос на восстановление пароля</p>
        <p>Ваш новый пароль - {{$password}}</p>
        @include('emails.footer')
    </body>
    </body>
</html>
