<!DOCTYPE html>
<html>
    <head>
        <title>Nivea</title>
    </head>
    <body>
        @include('emails.coverletterimage')
        <h3>{{$user->first_name}} {{$user->last_name}} хоче грати за Вашу команду</h3>
        <p>Вітаємо!</p>
        <p>Ваша команда зростає! Розгляньте запит на участь нового гравця у Заявочному листі.</p>
        <div id="button-wrapper" style="text-align: center;">
            <div id="button-block" style="display: inline-block;border: 1px solid #003566;background-color: #003566;border-radius: 1px;">
                <a href="{{$link}}" style="text-decoration:none">
                    <span class="button-text" style="padding: 30px;color: white;font-size: 3em; display:block;">Картка команди</span>
                </a>
            </div>
        </div>
        <p>            
            Нагадуємо, що у Вашій команді має бути не менше 6 і не більше 15 гравців.
            Гра скоро почнеться! Поспішайте зібрати усю команду.
        </p>
        @include('emails.footer')
    </body>
</html>
