<!DOCTYPE html>
<html>
    <head>
        <title>Nivea</title>
    </head>
    <body>
        @include('emails.coverletterimage')
        <h3>Ваш запит відхилено</h3>
        <p>
            Нажаль, капітан відхилив Ваш запит на участь в команді. Але немає причин сумувати: переходьте до загального переліку та обирайте команду, де Ви зможете проявити себе.                
        </p>
        <div id="button-wrapper" style="text-align: center;">
            <div id="button-block" style="display: inline-block;border: 1px solid #003566;background-color: #003566;border-radius: 1px;">
                <a href="" style="text-decoration:none">
                    <span class="button-text" style="padding: 30px;color: white;font-size: 3em; display:block;">Перейти на сайт</span>
                </a>
            </div>
        </div>
        <p>Або зберіть свою власну команду: заповніть заявочний лист команди та запросіть від 10 до 15 гравців. Нагадуємо, що формування команд триватиме до 31-го серпня.</p>
        <p>Бажаємо успіхів!</p>
        @include('emails.footer')
    </body>
    </body>
</html>
