<!DOCTYPE html>
<html>
    <head>
        <title>Nivea</title>
    </head>
    <body>
        @include('emails.coverletterimage')
        <h3>Ви – гравець команди {{$team->name}}</h3>
        <p>Вітаємо!</p>
        <p>Тепер Ви разом зі своєю командою готові до футбольних перемог!</p>
        <div id="button-wrapper" style="text-align: center;">
            <div id="button-block" style="isplay: inline-block;border: 1px solid #003566;background-color: #003566;border-radius: 1px;">
                <a href="{{$link}}" style="text-decoration:none">
                    <span class="button-text" style="padding: 30px;color: white;font-size: 3em; display:block;">Картка команди</span>
                </a>
            </div>
        </div>        
        <p>
            5-го вересня на сайті з’явиться Календар турніру NIVEA MEN 2016. Перші ігри відбудуться 10 та 11 вересня, та надалі щовихідних. Слідкуйте за календарем турніру, щоб не прогавити свою гру.
        </p>
        <p>Стежте за новинами, запитуйте та спілкуйтесь в офіційній спільноті турніру в Facebook.</p>
        <p>Якщо Ви бажаєте покинути свою команду, зверніться до капітана. Він має виключити Вас із заявочного листа.</p>
        <p>Бажаємо успіхів!</p>
        @include('emails.footer')
    </body>
    </body>
</html>
