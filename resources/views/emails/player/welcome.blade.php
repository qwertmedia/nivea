<!DOCTYPE html>
<html>
    <head>
        <title>Nivea</title>
    </head>
    <body>
        @include('emails.coverletterimage')
        <h3>Запит на участь у команді <НАЗВА КОМАНДИ> відправлено</h3>
        <p>Вітаємо!</p>
        <p>Ваш запит на участь знаходиться на розгляді в капітана команди. Ви отримаєте листа, щойно він затвердить Вашу кандидатуру.</p>
        <div id="button-wrapper" style="text-align: center;">
            <div id="button-block" style="display: inline-block;border: 1px solid #003566;background-color: #003566;border-radius: 1px;">
                <a href="{{$link}}" style="text-decoration:none">
                    <span class="button-text" style="padding: 30px;color: white;font-size: 3em; display:block;">Картка команди</span>
                </a>
            </div>
        </div>        
        <p>
            Нагадуємо, що згідно з правилами Турніру заявку на участь можна подати тільки в одну команду одночасно. Якщо капітан команди не відповідатиме на Вашу заявку впродрвж тривалого часу або відхилить Вашу кандидатуру, Ви зможете подати заявку в іншу команду. Для цього <a href="{{$linkDecline}}">скасуйте попередню</a> та оформіть нову заявку.
            Бажаємо успіхів!            
        </p>
        @include('emails.footer')
    </body>
    </body>
</html>
