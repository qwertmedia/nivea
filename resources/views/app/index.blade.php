<!DOCTYPE html>
<html>
    <head>
        <title>Nivea</title>
        <link rel="stylesheet" href="/css/front.css?time={{time()}}" media="all" charset="utf-8">
        <meta name="_token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div class="container">
            <div class="tabs">
                <a href="{{route('teamlist')}}" class="tabitem">Команди</a>
                <a href="/" class="tabitem">Зібрати Команду</a>
                <div class="pull-right">
                    <a href="{{route('front-login')}}" class="tabitem">Вхід для капітанів</a>
                </div>
            </div>
            @yield('content')
        </div>
        
    </body>
    </body>
</html>
