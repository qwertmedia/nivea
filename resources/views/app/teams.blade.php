@extends('app.index')

@section('content')
    <p class="boldtext1">КОМАНДИ</p>
    <p class="bigtext halfwidth">Зведений список команд заявок.</p>
    <table class="teamlist" width="100%">
        <thead>
            <tr>
                <th width="50%">Назва команд</th>
                <th width="30%">Капітан</th>
                <th>Гравці в команді</th>
            </tr>
        </thead>
        <tbody>
            @foreach($teams as $team)
                <tr>
                    <td>
                        @if($team->photo)
                            <img src="/images/small/{{$team->photo->path}}/{{$team->photo->name}}"/>
                        @endif

                        <a href="{{route('view-team', [$team->hashed_link])}}">{{$team->name}}</a>
                    </td>
                    <td>{{$team->captain->name}}</td>
                    <td>{{$team->players->count()}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
