@extends('app.index')

@section('content')
    <p class="boldtext1">ПОДАЧА ЗАЯВКИ<br/>НА УЧАСТЬ В ТУРНІРІ</p>
    <p class="bigtext halfwidth">Тількі командна заявка приймається до розгляду, тому перший крок це завести облікові записи команди, а вже по ходу справи наповнити її гравцями.</p>
    <div id="request"></div>
    <script type="text/javascript" src="/js/request.js?time={{time()}}"></script>
@endsection
