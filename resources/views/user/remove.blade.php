<!DOCTYPE html>
<html>
    <head>
        <title>Nivea</title>
        @if (($status == 'success') || !object_get($user, 'id'))
            <script>
                window.top.location.href = "{{env('APP_URL')}}";
            </script>
        @endif
    </head>
    <body>
        Remove user?
        <a href="{{ route('remove-user', [
                'link' => object_get($user, 'link'),
                'approved' => true
            ]) }}">Yes</a>
        <a href="{{ env('APP_URL') }}">No</a>
    </body>
    </body>
</html>
