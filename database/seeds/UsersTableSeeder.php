<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {

        $user = User::create([
            'first_name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => '11112222',
        ]);
        $user->attachRole(1);
    }
}
