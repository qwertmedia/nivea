<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Role::create([
            'name' => 'Super Admin',
            'slug' => 'superadmin',
            'level' => 1,
        ]);
        Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'level' => 2,
        ]);
        Role::create([
            'name' => 'Captain',
            'slug' => 'captain',
            'level' => 3,
        ]);
        Role::create([
            'name' => 'User',
            'slug' => 'user',
            'level' => 4,
        ]);
        Role::create([
            'name' => 'Player',
            'slug' => 'player',
            'level' => 4,
        ]);
    }
}
