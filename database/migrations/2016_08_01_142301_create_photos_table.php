<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('photos', function (Blueprint $table) {
             $table->increments('id');
             $table->string('name');
             $table->string('original_name');
             $table->string('path');
             $table->string('type');
             $table->string('meta_title');
             $table->string('meta_alt');
             $table->morphs('owner');
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('photos');
     }
}
