'use strict';

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ENV = process.env.NODE_ENV || 'development';
console.log(ENV);

let config = {
    context: path.resolve(__dirname, 'resources/assets'),
    entry: {
        auth: './admin/auth',
        admin: './admin/dashboard',
        front: './front'
    },
    output: {
        path: __dirname + '/public/js',
        filename: '[name].js',
        publicPath: (ENV == 'production' ? 'http://ext14.niveamen.ua/23/play_root/public/js/' : '/js/')
    },
    module: {
        loaders: [{
            test: /\.css$/,
            loader: ExtractTextPlugin.extract({
                fallbackLoader: 'style',
                loader: 'css'
            })

        },{
            test: /\.scss/,
            loaders: ExtractTextPlugin.extract({
                fallbackLoader: 'style',
                loader: ['css', 'sass']
            })

        }, {
            test: /\.(js|jsx)$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel',

        }, {
            test: /\.(svg|ttf|eot|woff|woff2|png)$/,
            loader: 'file?name=../fonts/[name].[ext]'
        }, {
            test: /\.(jpg|jpeg|gif|png)$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'url?limit=10000&name=../img/[name].[ext]'
        }, {
            test: /(dataTables\.net-bootstrap\/js|bootstrap\-fileinput\/js|vanilla\-masker)/,
            loader: 'imports?define=>false'
        }]
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.css', '.html', '.scss'],
        modules: [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'bower_components')],
        alias: {
            jquery: 'jquery/dist/jquery',
            noty: 'noty/js/noty/packaged/jquery.noty.packaged',
            'datatables.net': 'datatables.net/js/jquery.dataTables'
        },
        descriptionFiles: ['package.json', 'bower.json']
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
        }),
        new ExtractTextPlugin('../css/[name].css')
    ],
    externals: {
    },
    devtool: (ENV == 'production') ? null : '#eval'
};

if (ENV == 'production') {
    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            comments: false,
            compress: {
                warnings: false,
                unsafe: true,
                drop_console: false
            }
        })
    );
}

module.exports = config;
